package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class FluxMissilesArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.FLUX_MISSILES;

    public FluxMissilesArmament() {
        super(ARMAMENT_DRW);
    }
}
