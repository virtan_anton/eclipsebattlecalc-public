package com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings;


public enum DefaultDrawing {
    INTERCEPTOR(new ShipDrawing(
            3,  //INITIATIVE
            0,  //HULL
            0,  //FLUX_MISSILES
            0,  //PLASMA_MISSILES
            0,  //COMPUTER
            0,  //SHIELD
            1,  //ION_CANNON
            0,  //PLASMA_CANNON
            0,  //ANTIMATTER_CANNON
            0   //REGENERATION
    )),
    CRUISER (new ShipDrawing(
            2,  //INITIATIVE
            1,  //HULL
            0,  //FLUX_MISSILES
            0,  //PLASMA_MISSILES
            1,  //COMPUTER
            0,  //SHIELD
            1,  //ION_CANNON
            0,  //PLASMA_CANNON
            0,  //ANTIMATTER_CANNON
            0   //REGENERATION
    )),
    DREADNOUGHT (new ShipDrawing(
            1,  //INITIATIVE
            2,  //HULL
            0,  //FLUX_MISSILES
            0,  //PLASMA_MISSILES
            1,  //COMPUTER
            0,  //SHIELD
            2,  //ION_CANNON
            0,  //PLASMA_CANNON
            0,  //ANTIMATTER_CANNON
            0   //REGENERATION
    )),
    STARBASE (new ShipDrawing(
            4,  //INITIATIVE
            2,  //HULL
            0,  //FLUX_MISSILES
            0,  //PLASMA_MISSILES
            1,  //COMPUTER
            0,  //SHIELD
            1,  //ION_CANNON
            0,  //PLASMA_CANNON
            0,  //ANTIMATTER_CANNON
            0   //REGENERATION
    ));

    private ShipDrawing defaultShipDrawing;

    DefaultDrawing(ShipDrawing defaultShipDrawing) {
        this.defaultShipDrawing = defaultShipDrawing;
    }

    public ShipDrawing getDrawing() {
        return defaultShipDrawing;
    }
}
