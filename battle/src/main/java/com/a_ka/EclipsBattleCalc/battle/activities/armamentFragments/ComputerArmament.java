package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class ComputerArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.COMPUTER;


    public ComputerArmament() {
        super(ARMAMENT_DRW);
    }
}
