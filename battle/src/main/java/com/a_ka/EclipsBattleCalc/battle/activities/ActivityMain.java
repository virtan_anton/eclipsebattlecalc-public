package com.a_ka.EclipsBattleCalc.battle.activities;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.ShipDrawing;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments.ShipFParams;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments.ShipFragment;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments.ShipFragment.onFragmentClickListener;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Cruiser;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Dreadnought;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Interceptor;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Starbase;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ActivityMain extends ActionBarActivity
        implements onFragmentClickListener,
        TabParty.PartyParamChangeListener,
        Tab3.ResultUpdater,
        CalcRetainedFragment.TaskCallbacksListener {


    LinearLayout mMainLinearLayout;
    Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Party 1", "Party 2", "Battle"};
    int NumbOfTabs = 3;
    int mPreviousPosition = 0;
    FragmentManager fManager;

    FloatingActionMenu fabMenu;
    FloatingActionButton btn11, btn12, btn13, btn14;

    FloatingActionButton mFabCalc;
    int mCurrentFabProgress;
    Handler mUiHandler = new Handler();

    final String RETAINED_FRAGMENT_TAG = "RETAINED_FRAGMENT_TAG";

    Fragment currentFragm;
    Tab3 mTab3;
    CalcRetainedFragment calcFragment;

    List<Fragment> mParty1NestedFragmentList;
    List<Fragment> mParty2NestedFragmentList;
    int mParty1OptionFlag = 1;
    int mParty2OptionFlag = 0;

    Party mP1;
    Party mP2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onCreate");
        }

        if (savedInstanceState != null) {
            mPreviousPosition = savedInstanceState.getInt("previousPosition");
            if (Logs.showLogs) {
                Log.d(Tags.MY_TAG.getTag(), "onCreate(), mPreviousPosition =  " + mPreviousPosition);
            }
            mCurrentFabProgress = savedInstanceState.getInt("mCurrentFabProgress");
            mParty1OptionFlag = savedInstanceState.getInt("mParty1OptionFlag");
            mParty2OptionFlag = savedInstanceState.getInt("mParty2OptionFlag");
        }


        fManager = getSupportFragmentManager();

        if (fManager.findFragmentByTag(RETAINED_FRAGMENT_TAG) == null) {
            if (Logs.showLogs) {
                Log.d(Tags.MY_TAG.getTag(), "add calcFragment");
            }
            fManager.beginTransaction().add(new CalcRetainedFragment(), RETAINED_FRAGMENT_TAG).commit();
            fManager.executePendingTransactions();
        }

        calcFragment = (CalcRetainedFragment) getSupportFragmentManager().findFragmentByTag(RETAINED_FRAGMENT_TAG);

        if (calcFragment == null) if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "calcFragment == null");
        }

        mParty1NestedFragmentList = calcFragment.getParty1NestedFragmentList();
        mParty2NestedFragmentList = calcFragment.getParty2NestedFragmentList();

        mMainLinearLayout = (LinearLayout) findViewById(R.id.main_linear_layout);
        mFabCalc = (FloatingActionButton) findViewById(R.id.fab_battle);
        mFabCalc.setMax(100);
        mFabCalc.hide(false);
        fabMenu = (FloatingActionMenu) findViewById(R.id.menu1);
        fabMenu.setClosedOnTouchOutside(true);
        btn11 = (FloatingActionButton) findViewById(R.id.add_interceptor);
        btn12 = (FloatingActionButton) findViewById(R.id.add_cruiser);
        btn13 = (FloatingActionButton) findViewById(R.id.add_dreadnought);
        btn14 = (FloatingActionButton) findViewById(R.id.add_starbase);


        // Creating The Toolbar and setting it as the Toolbar for the activity

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, NumbOfTabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // make previous tab's fragment do onPauseFragment()
                if (Logs.showLogs) {
                    Log.d(Tags.MY_TAG.getTag(), "onPageSelected(), position =  " + position);
                    Log.d(Tags.MY_TAG.getTag(), "onPageSelected(), mPreviousPosition =  " + mPreviousPosition);
                }
                FragmentLifecycle previousFragment = (FragmentLifecycle) adapter.instantiateItem(pager, mPreviousPosition);
                previousFragment.onPauseFragment();
                FragmentLifecycle currentFragment = (FragmentLifecycle) adapter.instantiateItem(pager, position);
                currentFragment.onResumeFragment();
                currentFragm = (Fragment) adapter.instantiateItem(pager, position);
                switch (position) {
                    case 0:
                        mFabCalc.hide(false);
                        fabMenu.hideMenuButton(false);
                        fabMenu.showMenuButton(true);
                        break;
                    case 1:
                        mFabCalc.hide(false);
                        fabMenu.hideMenuButton(false);
                        fabMenu.showMenuButton(true);
                        break;
                    case 2:
                        fabMenu.hideMenuButton(false);
                        mFabCalc.show(true);
                        break;
                }

                mPreviousPosition = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        btn11.setOnClickListener(new OnFAMClickListener(pager, ShipFParams.INTERCEPTOR));
        btn12.setOnClickListener(new OnFAMClickListener(pager, ShipFParams.CRUISER));
        btn13.setOnClickListener(new OnFAMClickListener(pager, ShipFParams.DREADNOUGHT));
        btn14.setOnClickListener(new OnFAMClickListener(pager, ShipFParams.STARBASE));

        mFabCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTask task = calcFragment.getAsyncTask();
                if (hasPartiesShips() && (task == null || task.getStatus() != AsyncTask.Status.RUNNING)) {
                    calcFragment.startAsyncCalc(mP1, mP2);
                }

            }
        });
    }

    @Override
    public void onPartyOptionChanged(int partyPositionInPager) {
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "partyPositionInPager: " + partyPositionInPager);
        }
        TabParty mainParty = (TabParty) adapter.instantiateItem(pager, partyPositionInPager);
        TabParty changingParty;
        int changingFlag;
        int basicFlag;
        if (partyPositionInPager == 0) {
            changingParty = (TabParty) adapter.instantiateItem(pager, 1);
            basicFlag = mParty1OptionFlag;
            changingFlag = mParty2OptionFlag;
        } else {
            changingParty = (TabParty) adapter.instantiateItem(pager, 0);
            basicFlag = mParty2OptionFlag;
            changingFlag = mParty1OptionFlag;
        }

        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "mainParty optionFlag: " + basicFlag);
            Log.d(Tags.MY_TAG.getTag(), "changingParty optionFlag: " + changingFlag);
        }
        if ((basicFlag & PartyOptions.DEFENDER.getOptionFlag()) == (changingFlag & PartyOptions.DEFENDER.getOptionFlag())) {
            if ((changingFlag & PartyOptions.DEFENDER.getOptionFlag()) == PartyOptions.DEFENDER.getOptionFlag()) {
                changingFlag = changingFlag ^ PartyOptions.DEFENDER.getOptionFlag();
            } else {
                changingFlag = changingFlag | PartyOptions.DEFENDER.getOptionFlag();
            }
        }
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "mainParty optionFlag: " + basicFlag);
            Log.d(Tags.MY_TAG.getTag(), "changingParty optionFlag: " + changingFlag);
        }

        if (partyPositionInPager == 0) {
            mParty1OptionFlag = basicFlag;
            mParty2OptionFlag = changingFlag;
        } else {
            mParty2OptionFlag = basicFlag;
            mParty1OptionFlag = changingFlag;
        }

        mainParty.setPartyOptionsFlag(basicFlag);
        changingParty.setPartyOptionsFlag(changingFlag);

    }

    @Override
    public void putPartyOptionFlag(int partyOptionFlag, int positionInPager) {
        if (positionInPager == 0) mParty1OptionFlag = partyOptionFlag;
        else mParty2OptionFlag = partyOptionFlag;
    }

    @Override
    public void onPartyNestedFragmentListChanged(int partyPositionInPager) {
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "onPartyNestedFragmentListChanged(), partyPositionInPager: " + partyPositionInPager);
        }
        int iTab = partyPositionInPager;
        FragmentLifecycle fLifecycle;
        TabParty changedPartyTab = (TabParty) adapter.instantiateItem(pager, iTab);
        List<Fragment> fragmentList = changedPartyTab.getChildFragmentManager().getFragments();
        if (fragmentList != null) {
            if (iTab == 0) {
                mParty1OptionFlag = changedPartyTab.getPartyOptionsFlag();
                mParty1NestedFragmentList.clear();
                for (Fragment fragment : fragmentList) {
                    if (fragment != null && fragment.isVisible()) {
                        fLifecycle = (FragmentLifecycle) fragment;
                        fLifecycle.onPauseFragment();
                        updatePartyMissilesFlag(Party.PartyTag.PARTY1, (ShipFragment) fragment);
                        mParty1NestedFragmentList.add(fragment);
                    }
                }
            }
            if (iTab == 1) {
                mParty2OptionFlag = changedPartyTab.getPartyOptionsFlag();
                mParty2NestedFragmentList.clear();
                for (Fragment fragment : fragmentList) {
                    if (fragment != null && fragment.isVisible()) {
                        fLifecycle = (FragmentLifecycle) fragment;
                        fLifecycle.onPauseFragment();
                        updatePartyMissilesFlag(Party.PartyTag.PARTY2, (ShipFragment) fragment);
                        mParty2NestedFragmentList.add(fragment);
                    }
                }
            }
        }
    }

    private void updatePartyMissilesFlag(Party.PartyTag party, ShipFragment fragment) {
        ShipDrawing drawing = fragment.getShipDrawing();
        if (drawing.hasMissiles()) {
            if (party == Party.PartyTag.PARTY1) {
                mParty1OptionFlag = mParty1OptionFlag | Party.FLAG_HAS_MISSILES;
            } else mParty2OptionFlag = mParty2OptionFlag | Party.FLAG_HAS_MISSILES;

        }
    }

    // return false if data not changed
    public boolean isNewParties() {
        Party calcP1 = calcFragment.getCalculatedParty1();
        Party calcP2 = calcFragment.getCalculatedParty2();
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "calcP1: " + calcP1);
            Log.d(Tags.MY_TAG.getTag(), "calcP2: " + calcP2);
        }
        Party newP1 = getParty(Party.PartyTag.PARTY1);
        Party newP2 = getParty(Party.PartyTag.PARTY2);
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "newP1: " + newP1);
            Log.d(Tags.MY_TAG.getTag(), "newP2: " + newP2);
        }
        boolean isNewParties = !(compareParties(calcP1, newP1) && compareParties(calcP2, newP2));
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), "isNewParties? " + isNewParties);
        }
        return isNewParties;
    }

    private boolean hasPartiesShips() {
        Party p1 = getParty(Party.PartyTag.PARTY1);
        Party p2 = getParty(Party.PartyTag.PARTY2);
        if (p1.getShipList().size() == 0) {
            if (Logs.showLogs) {
                Log.d(Tags.MY_TAG.getTag(), "HAS NO SHIPS!: " + p1);
            }
            Toast.makeText(ActivityMain.this, R.string.error_no_ships_party1, Toast.LENGTH_LONG).show();
            return false;
        } else {
            if (p2.getShipList().size() == 0) {
                if (Logs.showLogs) {
                    Log.d(Tags.MY_TAG.getTag(), "HAS NO SHIPS!: " + p2);
                }
                Toast.makeText(ActivityMain.this, R.string.error_no_ships_party2, Toast.LENGTH_LONG).show();
                return false;
            } else {
                if (Logs.showLogs) {
                    Log.d(Tags.MY_TAG.getTag(), "PARTIES IS OK!: " + p1 + " " + p2);
                }
                mP1 = p1;
                mP2 = p2;
                return true;
            }
        }
    }

    private boolean compareParties(Party p1, Party p2) {
        // compare first 4 bits
        boolean isEqual = ((p1.getFLAG() == p2.getFLAG()) && compareShipLists(p1.getShipList(), p2.getShipList()));
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), " isEqual: " + p1 + " and " + p2 + "? " + isEqual);
        }
        return isEqual;
    }

    private boolean compareShipLists(List<Ship> shipList1, List<Ship> shipList2) {
        if (shipList1.size() == shipList2.size()) {
            Collections.sort(shipList1);
            Collections.sort(shipList2);
            int iList1 = 0;
            int iList2 = 0;
            while (iList1 < shipList1.size()) {
                while (iList2 < shipList2.size()) {
                    if (shipList1.get(iList1).equals(shipList2.get(iList2))) {
                        iList1++;
                        iList2++;
                    } else return false;
                }
            }
            return true;
        }
        return false;
    }

    private class OnFAMClickListener implements View.OnClickListener {

        private ViewPager pager;
        private ViewPagerAdapter adapter;
        private ShipFParams shipType;
        private Fragment tempFragm;
        private Fragment currentFragm;
        private FragmentManager currentFragmManager;
        private String SHIP_TAG_1;
        private String SHIP_TAG_2;


        public OnFAMClickListener(ViewPager pager, ShipFParams ship) {
            this.pager = pager;
            this.shipType = ship;
        }

        @Override
        public void onClick(View v) {
            ShipFragment shipFragment;
            adapter = (ViewPagerAdapter) pager.getAdapter();
            int position = pager.getCurrentItem();

            SHIP_TAG_1 = shipType.getSHIP_TAG_1();
            SHIP_TAG_2 = shipType.getSHIP_TAG_2();


            switch (position) {
                case 0:

                    currentFragm = (Fragment) adapter.instantiateItem(pager, position);
                    currentFragmManager = currentFragm.getChildFragmentManager();
                    tempFragm = currentFragmManager.findFragmentByTag(SHIP_TAG_1);
                    if (tempFragm == null) {
                        shipFragment = (ShipFragment) ShipFragment.instantiate(ActivityMain.this, shipType.getShipFname());
                        if (Logs.showLogs) {
                            Log.d("MY_TAG", "shipType.toString: " + shipType.toString());
                            Log.d("MY_TAG", "pagerAdapterPosition: " + position);
                        }
                        shipFragment.setPositionInPager(position);
                        shipFragment.setFragmTag(SHIP_TAG_1);
                        currentFragmManager.beginTransaction().add(R.id.fragmentContainer, shipFragment, SHIP_TAG_1).addToBackStack(null).commit();
                        fabMenu.close(true);
                    } else {
                        if (tempFragm.isDetached()) {
                            currentFragmManager.beginTransaction().attach(tempFragm).addToBackStack(null).commit();
                            fabMenu.close(true);
                        } else {
                            fabMenu.close(true);
                        }
                    }
                    break;
                case 1:
                    currentFragm = (Fragment) adapter.instantiateItem(pager, position);
                    currentFragmManager = currentFragm.getChildFragmentManager();
                    tempFragm = currentFragmManager.findFragmentByTag(SHIP_TAG_2);
                    if (tempFragm == null) {
                        shipFragment = (ShipFragment) ShipFragment.instantiate(ActivityMain.this, shipType.getShipFname());
                        if (Logs.showLogs) {
                            Log.d("MY_TAG", "shipType.toString: " + shipType.toString());
                            Log.d("MY_TAG", "pagerAdapterPosition: " + position);
                        }
                        shipFragment.setPositionInPager(position);
                        shipFragment.setFragmTag(SHIP_TAG_2);
                        currentFragmManager.beginTransaction().add(R.id.fragmentContainer, shipFragment, SHIP_TAG_2).addToBackStack(null).commit();
                        fabMenu.close(true);
                    } else {
                        if (tempFragm.isDetached()) {
                            currentFragmManager.beginTransaction().attach(tempFragm).addToBackStack(null).commit();
                            fabMenu.close(true);
                        } else {
                            fabMenu.close(true);
                        }
                    }
                    break;
            }

        }
    }

    @Override
    protected void onStart() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onStart");
        }
        super.onStart();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onRestoreInstanceState");
        }
        super.onRestoreInstanceState(savedInstanceState);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), this.getClass() + " onActivityResult");
            Log.d(Tags.MY_TAG.getTag(), this.getClass() + " requestCode: " + requestCode);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onResume");
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onPause");
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onStop");
        }
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onSaveInstanceState");
            Log.d(Tags.MY_TAG.getTag(), "onSaveInstanceState(),  previousPosition = " + mPreviousPosition);
        }
        outState.putInt("previousPosition", mPreviousPosition);
        calcFragment.putParty1NestedFragmentList(mParty1NestedFragmentList);
        calcFragment.putParty2NestedFragmentList(mParty2NestedFragmentList);
        outState.putInt("mCurrentFabProgress", mCurrentFabProgress);
        outState.putInt("mParty1OptionFlag", mParty1OptionFlag);
        outState.putInt("mParty2OptionFlag", mParty2OptionFlag);
    }

    @Override
    protected void onDestroy() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onDestroy");
        }
        super.onDestroy();
    }

    public void deleteFragment(String fragmentTag, int pagerPosition) {
        Fragment tempFragm;
        currentFragm =  adapter.getRegisteredFragment(pagerPosition);
        tempFragm = currentFragm.getChildFragmentManager().findFragmentByTag(fragmentTag);
        currentFragm.getChildFragmentManager().beginTransaction().detach(tempFragm).commit();
    }


    @Override
    public void onBackPressed() {
        if(fabMenu.isOpened()){
            fabMenu.close(true);
        } else super.onBackPressed();
    }


    @Override
    public void onFragmentLongClickEvent(int positionInPager, String fragmTag) {
        deleteFragment(fragmTag,positionInPager);
        Toast.makeText(this,R.string.delete_ship_toast,Toast.LENGTH_SHORT).show();
    }

    public Party getParty(Party.PartyTag partyTag) {
        int partyOptionFlag = 0;
        List<Ship> partyShipList = new ArrayList<>();
        if (partyTag == Party.PartyTag.PARTY1) {
            partyOptionFlag = mParty1OptionFlag;
            partyShipList = makePartyShipList(mParty1NestedFragmentList);
        }
        if (partyTag == Party.PartyTag.PARTY2) {
            partyOptionFlag = mParty2OptionFlag;
            partyShipList = makePartyShipList(mParty2NestedFragmentList);
        }
        return new Party(partyOptionFlag, partyShipList, partyTag);
    }

    private List<Ship> makePartyShipList(List<Fragment> partyNestedFragmentList) {
        List<Ship> shipList = new ArrayList<>();
        ShipFragment shipFragment;
        for (Fragment fragment : partyNestedFragmentList) {
//            Log.d(Tags.MY_TAG.getTag(), " makePartyShipList():  Yo, wi have a fragment!");
            if (fragment != null) {
//                Log.d(Tags.MY_TAG.getTag(), " makePartyShipList():  Seem it's a Ship...");
                shipFragment = (ShipFragment) fragment;
//                Log.d(Tags.MY_TAG.getTag(), " makePartyShipList():  Yes! It's Ship!");
                int shipAmount = shipFragment.getShipAmount();
//                Log.d(Tags.MY_TAG.getTag(), " makePartyShipList():  There is(are) " + shipAmount + " ship(s)");
                int i = 0;
                while (i < shipAmount) {
                    shipList.add(getShipFromFragment(shipFragment));
                    i++;
                }
            }
        }
        return shipList;
    }

    private Ship getShipFromFragment(ShipFragment shipFragment) {
        Ship ship;
        ShipDrawing shipDrawing = shipFragment.getShipDrawing();
        switch (shipFragment.getShipType()) {
            default:
                ship = new Interceptor();
                ship.installDrawing(shipDrawing);
                return ship;
            case CRUISER:
                ship = new Cruiser();
                ship.installDrawing(shipDrawing);
                break;
            case DREADNOUGHT:
                ship = new Dreadnought();
                ship.installDrawing(shipDrawing);
                break;
            case STARBASE:
                ship = new Starbase();
                ship.installDrawing(shipDrawing);
                break;
        }
        return ship;
    }

    @Override
    public void onPreExecute() {
        mCurrentFabProgress = 0;
    }


    @Override
    public void onProgressUpdate(int percent) {
        increaseFabProgress(percent, mFabCalc);
    }

    private void increaseFabProgress(final int taskProgress, final FloatingActionButton fab) {
        if (taskProgress <= fab.getMax()) {
            if (mCurrentFabProgress <= taskProgress) {
                fab.setProgress(mCurrentFabProgress, false);
                mCurrentFabProgress++;
                mUiHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        increaseFabProgress(taskProgress, fab);
                    }
                }, 30);
            }
        }
    }

    @Override
    public void onPostExecute() {
        mTab3 = (Tab3) adapter.instantiateItem(pager, 2);
        mTab3.publishBattleResult(calcFragment.getCalculatedPartiesWinPrc(), calcFragment.getCalculatedParty1ExpectedLoss(), calcFragment.getCalculatedParty2ExpectedLoss());
    }

    @Override
    public void updateResult() {
        if (calcFragment.isCalculatedResult() && isNewParties()) {
            mTab3 = (Tab3) adapter.instantiateItem(pager, 2);
            mTab3.clearResult();
            mFabCalc.hideProgress();
        }
    }


}


