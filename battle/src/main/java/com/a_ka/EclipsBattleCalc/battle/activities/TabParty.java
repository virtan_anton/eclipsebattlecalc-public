package com.a_ka.EclipsBattleCalc.battle.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.R;
import com.github.clans.fab.FloatingActionMenu;

import java.util.List;

public abstract class TabParty extends Fragment implements FragmentLifecycle {

    private int partyOldScrollY = 0;
    private FloatingActionMenu menu1;
    private LinearLayout partyOptions;
    private int partyStringResId;
    private int positionInPager;

    private int mPartyOptionsFlag;


    public interface PartyParamChangeListener {
        void putPartyOptionFlag(int partyOptionFlag, int positionInPager);

        void onPartyOptionChanged(int partyPositionInPager);

        void onPartyNestedFragmentListChanged(int partyPositionInPager);
    }


    PartyParamChangeListener paramChangeListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            paramChangeListener = (PartyParamChangeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement PartyParamChangeListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mPartyOptionsFlag = savedInstanceState.getInt("mPartyOptionsFlag");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_1_2, container, false);
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onCreateView");
        }

        partyOptions = (LinearLayout) v.findViewById(R.id.party_options);
        partyOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ActivityPartyOptions.class);
                intent.putExtra("mPartyOptionsFlag", mPartyOptionsFlag);
                intent.putExtra("titleResId", partyStringResId);
                startActivityForResult(intent, 1);
            }
        });


        menu1 = (FloatingActionMenu) getActivity().findViewById(R.id.menu1);

        final ScrollView partyShipScrollView = (ScrollView) v.findViewById(R.id.ship_scroll_list);
        partyShipScrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        partyShipScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = partyShipScrollView.getScrollY();
                if (scrollY != partyOldScrollY) {
                    int scrollDY = partyOldScrollY - scrollY;
                    if (scrollDY < 0) {
                        menu1.setMenuButtonHideAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_out_bottom));
                        menu1.hideMenuButton(true);
                        menu1.setMenuButtonHideAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_scale_down));
                    }
                    if (scrollDY > 0) {
                        menu1.setMenuButtonShowAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_in_bottom));
                        menu1.showMenuButton(true);
                        menu1.setMenuButtonShowAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_scale_up));
                    }
                    partyOldScrollY = scrollY;
                }
            }
        });

        return v;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onStart");
        }
        LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout partyOptionsContainer = (LinearLayout) partyOptions.findViewById(R.id.party_options_container);
        partyOptionsContainer.removeAllViews();
        for (PartyOptions option : PartyOptions.values()) {
            if ((mPartyOptionsFlag & option.getOptionFlag()) == option.getOptionFlag()) {
                View partyOptionView = inflater.inflate(R.layout.party_option_img, null);
                ImageView partyOptionImg = (ImageView) partyOptionView.findViewById(R.id.option_img);
                partyOptionImg.setImageResource(option.getOptionImgRes());
                partyOptionsContainer.addView(partyOptionView);
            }
        }
    }

    @Override
    public void onResume() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onResume");
        }
        super.onResume();
    }

    @Override
    public void onResumeFragment() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), this.getClass() + " onActivityResult");
            Log.d(Tags.MY_TAG.getTag(), this.getClass() + " requestCode: " + requestCode);
        }
        super.onActivityResult(requestCode, resultCode, data);

        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }

        if (resultCode == Activity.RESULT_OK && data != null) {
            paramChangeListener.putPartyOptionFlag(data.getIntExtra("mPartyOptionsFlag", mPartyOptionsFlag), positionInPager);
            if (Logs.showLogs) {
                Log.d(Tags.MY_TAG.getTag(), this.getClass() + " mPartyOptionsFlag: " + mPartyOptionsFlag);
            }
            paramChangeListener.onPartyOptionChanged(positionInPager);
        }

    }

    @Override
    public void onPauseFragment() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onPauseFragment");
        }
        paramChangeListener.onPartyNestedFragmentListChanged(positionInPager);
    }

    @Override
    public void onPause() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onPause");
        }
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onSaveInstanceState");
        }
        super.onSaveInstanceState(outState);
        outState.putInt("mPartyOptionsFlag", mPartyOptionsFlag);
    }

    @Override
    public void onStop() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onStop");
        }
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onDestroyView");
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onDestroy");
        }
        super.onDestroy();
    }


    public int getPartyOptionsFlag() {
        return mPartyOptionsFlag;
    }

    public void setPartyOptionsFlag(int mPartyOptionsFlag) {
        this.mPartyOptionsFlag = mPartyOptionsFlag;
    }

    public void addPartyOption(int PARTY_FLAG) {
        this.mPartyOptionsFlag = mPartyOptionsFlag | PARTY_FLAG;
    }

    public void setPartyStringResId(int partyStringResId) {
        this.partyStringResId = partyStringResId;
    }

    public void setPositionInPager(int positionInPager) {
        this.positionInPager = positionInPager;
    }


}
