package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class RegenerationArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.REGENERATION;

    public RegenerationArmament() {
        super(ARMAMENT_DRW);
    }
}
