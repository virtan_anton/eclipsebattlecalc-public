package com.a_ka.EclipsBattleCalc.battle.main;

import android.util.Log;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.Shell;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class AimSelection {

    // set for ship shells that can hit it
    public static void setShipsPotentialShells(List<Shell> shellList, Party targetParty, Class battlePhase) {
        for (Ship ship : targetParty.getShipList()) {
            ship.getPotentialShells().clear();
            for (Shell shell : shellList) {
                if (isShellHitShip(shell, ship, targetParty, battlePhase)) {
                    ship.addPotentialShell(shell);
                    shell.setHasPotentialAim(true);
                }
            }
        }
    }

    private static boolean isShellListHitShip(Ship ship) {
        //sort shells. If has aim - remove
        sortPotentialShellsList(ship.getPotentialShells());
        List<Shell> shipPotentialShells = ship.getPotentialShells();
        int dmgSum = 0;
        for (Shell shell : shipPotentialShells) {
            dmgSum += shell.getDamage();
        }
        return dmgSum >= ship.getHull() + 1 - ship.getObtainedDmg();
    }


    public static void selectShells(SelectionType type, Ship ship) {
        switch (type) {
            case DESTROY:
                if (isShellListHitShip(ship)) {
                    if (Logs.showLogs) {
                        Log.d(Tags.CALC.getTag(), "selectShells(), isShellListHitShip: true");
                    }
                    int potentialShellAmount = ship.getPotentialShells().size();
                    if (Logs.showLogs) {
                        Log.d(Tags.CALC.getTag(), "selectShells(), potentialShellAmount: " + potentialShellAmount);
                    }
                    for (int iShellAmount = 1; iShellAmount < (potentialShellAmount + 1); iShellAmount++) {
                        if (isNShellsDestroyShip(ship, iShellAmount)) {
                            if (Logs.showLogs) {
                                Log.d(Tags.CALC.getTag(), "selectShells(). " + iShellAmount + " shell(s) may destroy ship");
                            }
                            selectNShells(ship, iShellAmount);
                            break;
                        }
                    }
                }
                break;
            case DAMAGE:
                assignAimForRemainShells(ship);
                break;
        }
    }

    public enum SelectionType {DESTROY, DAMAGE}

    private static boolean isNShellsDestroyShip(Ship ship, int shellsN) {
        // sort shells by DMG
        Collections.sort(ship.getPotentialShells());
        List<Shell> potentialShells = ship.getPotentialShells();
        int dmgSum = 0;
        for (int iShellPos = 0; iShellPos < shellsN; iShellPos++) {
            dmgSum += potentialShells.get(iShellPos).getDamage();
        }
        return dmgSum >= ship.getHull() + 1 - ship.getObtainedDmg();
    }

    private static void selectNShells(Ship ship, int shellsN) {
        List<Shell> potentialShells = ship.getPotentialShells();
        int overkill;
        int dmgSum = 0;
        for (int iShell = 0; iShell < shellsN; iShell++) {
            dmgSum += potentialShells.get(iShell).getDamage();
            potentialShells.get(iShell).setIsIndicativeAim(true);
        }
        overkill = ship.getObtainedDmg() + dmgSum - (ship.getHull() + 1);
        if (overkill < 0) {
            throw new IllegalArgumentException("overkill < 0");
        }
        if (overkill == 0) {
            assignAimForPotentialShells(ship);
        } else {
            tryFindSubstitute(ship, shellsN, overkill);
        }
    }

    private static void tryFindSubstitute(Ship ship, int shellsN, int overkill) {
        List<Shell> shellsList = ship.getPotentialShells();
        int shellsListOverLength = shellsList.size() - shellsN;
        if (shellsListOverLength < 0)
            throw new IllegalArgumentException("shellsListOverLength < 0");
        switch (overkill) {
            case 3:
                if (shellsListOverLength >= 1) {
                    if (findSubstituteShellPosition(shellsList, 1) >= 0) {
                        deselectLastShellOfShellsN(shellsList, shellsN);
                        assignAimForPotentialShells(ship);
                        break;
                    } else {
                        if (findSubstituteShellPosition(shellsList, 2) >= 0) {
                            deselectLastShellOfShellsN(shellsList, shellsN);
                            assignAimForPotentialShells(ship);
                            break;
                        }
                    }
                }
                assignAimForPotentialShells(ship);
                break;

            case 2:
                if (shellsListOverLength >= 1) {
                    if (findSubstituteShellPosition(shellsList, 2) >= 0) {
                        deselectLastShellOfShellsN(shellsList, shellsN);
                        assignAimForPotentialShells(ship);
                        break;
                    } else {
                        if (shellsListOverLength >= 2) {
                            if (findSubstituteShellPosition(shellsList, 1) >= 0
                                    && findSubstituteShellPosition(shellsList, 1) >= 0) {
                                deselectLastShellOfShellsN(shellsList, shellsN);
                                assignAimForPotentialShells(ship);
                                break;
                            }
                        }
                    }
                }
                assignAimForPotentialShells(ship);
                break;

            case 1:
                if (shellsListOverLength >= 2) {
                    if (findSubstituteShellPosition(shellsList, 2) >= 0
                            && findSubstituteShellPosition(shellsList, 1) >= 0) {
                        deselectLastShellOfShellsN(shellsList, shellsN);
                        assignAimForPotentialShells(ship);
                        break;
                    } else {
                        if (shellsListOverLength >= 3) {
                            if (findSubstituteShellPosition(shellsList, 1) >= 0
                                    && findSubstituteShellPosition(shellsList, 1) >= 0
                                    && findSubstituteShellPosition(shellsList, 1) >= 0) {
                                deselectLastShellOfShellsN(shellsList, shellsN);
                                assignAimForPotentialShells(ship);
                                break;
                            }
                        }
                    }
                }
                assignAimForPotentialShells(ship);
                break;
        }
    }

    private static int findSubstituteShellPosition(List<Shell> shellList, int shellDmg) {
        int position = -1;
        for (int iShell = 0; iShell < shellList.size(); iShell++) {
            Shell shell = shellList.get(iShell);
            if (!shell.isIndicativeAim() && shell.getDamage() == shellDmg) {
                shell.setIsIndicativeAim(true);
                position = iShell;
                break;
            }
        }
        return position;
    }

    private static void deselectLastShellOfShellsN(List<Shell> shellList, int shellsN) {
        shellList.get(shellsN - 1).setIsIndicativeAim(false);
    }

    private static void assignAimForPotentialShells(Ship ship) {
        List<Shell> shellList = ship.getPotentialShells();
        for (Shell shell : shellList) {
            if (shell.isIndicativeAim()) {
                shell.setAim(ship.getID());
                ship.addSelectedShell(shell);
                ship.setFLAG(ship.getFLAG() | Ship.FLAG_DESTROYED);
            }
        }
    }

    private static void assignAimForRemainShells(Ship ship) {
        if (!((ship.getFLAG() & Ship.FLAG_DESTROYED) == Ship.FLAG_DESTROYED)) {
            for (Shell shell : ship.getPotentialShells()) {
                if (shell.getAim() == 0) {
                    shell.setAim(ship.getID());
                    ship.addSelectedShell(shell);
                }
            }
        }
    }


    //
    // remove from List<Shell> shells with selected aim
    public static void sortPotentialShellsList(List<Shell> shellList) {
        for (Iterator<Shell> it = shellList.iterator(); it.hasNext(); ) {
            if (it.next().getAim() != 0) it.remove();
        }
    }

    // check shell goal
    private static boolean isShellHitShip(Shell shell, Ship ship, Party targetParty, Class battlePhase) {
        if (shell.getDiceValue() == 6) {
            return true;
        }
        if (shell.getDiceValue() == 1) {
            return false;
        }
        int partiesFLAG_DESTORTION_SHIELD_BONUS = getPartiesFLAG_DESTORTION_SHIELD_BONUS(targetParty, battlePhase);
        return shell.getDiceValue() + shell.getComputerValue() - ship.getShield() + partiesFLAG_DESTORTION_SHIELD_BONUS >= 6;
    }


    private static int getPartiesFLAG_DESTORTION_SHIELD_BONUS(Party targetParty, Class battlePhase) {
        if (battlePhase == MissilePhase.class) {
            return targetParty.getFLAG_DISTORTION_SHIELD_BONUS();
        }
        return 0;
    }


}
