package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class PlasmaMissilesArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.PLASMA_MISSILES;

    public PlasmaMissilesArmament() {
        super(ARMAMENT_DRW);
    }
}
