package com.a_ka.EclipsBattleCalc.battle.pojo.ships;


public class Cruiser extends Ship {

    public Cruiser() {
        super();
        this.setIonCannon(1);
        this.setHull(1);
        this.setInitiative(2);
        this.setComputer(1);
        this.setShipType(ShipType.CRUISER);
    }
}
