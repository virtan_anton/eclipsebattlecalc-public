package com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.FragmentLifecycle;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.ShipDrawing;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities.CruiserActivity;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities.DreadnoughtActivity;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities.InterceptorActivity;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities.StarbaseActivity;

public abstract class ShipFragment extends Fragment implements FragmentLifecycle {

    View rootView;
    private ImageView shipImageView;
    private TextView shipText;
    private String fragmTag;
    FragmentActivity activity;

    private int positionInPager;
    private int shipImgID;
    private int shipStringID;
    private String[] spinnerShipAmountArray;
    private ShipFParams shipType;
    private int FRAGMENT_REQUEST_CODE;
    private ShipDrawing shipDrawing;

    Spinner spinnerShipsAmount;
    private int shipAmount;


    public ShipFragment(ShipFParams shipType) {
        this.shipType = shipType;
        this.shipImgID = shipType.getIMG_SRC();
        this.shipStringID = shipType.getNAME_SRC();
        this.spinnerShipAmountArray = shipType.getSHIP_AMOUNT_LIST();
        this.shipDrawing = shipType.getDefaultShipDrawing().getDrawing();
    }

    public void setFragmTag(String fragmTag) {
        this.fragmTag = fragmTag;
        String[] tempArray = fragmTag.split("-");
        FRAGMENT_REQUEST_CODE = Integer.parseInt(tempArray[1]);
    }

    public interface onFragmentClickListener {

        void onFragmentLongClickEvent(int positionInPager, String fragmTag);
    }

    onFragmentClickListener clickListener;


    public void setPositionInPager(int positionInPager) {
        this.positionInPager = positionInPager;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity =(FragmentActivity) activity;
        try {
            clickListener = (onFragmentClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement onFragmentClickListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            FRAGMENT_REQUEST_CODE = savedInstanceState.getInt("FRAGMENT_REQUEST_CODE");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ship, container, false);
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onCreateView");
        }

        shipImageView = (ImageView) rootView.findViewById(R.id.imageView);
        shipText = (TextView) rootView.findViewById(R.id.textView);

        shipImageView.setImageResource(shipImgID);
        shipText.setText(shipStringID);

        rootView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                clickListener.onFragmentLongClickEvent(positionInPager, fragmTag);
                return true;
            }
        });

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (shipType) {
                    case INTERCEPTOR:
                        intent = new Intent(activity, InterceptorActivity.class);
                        intent.putExtra("shipDrawing", shipDrawing);
                        getParentFragment().startActivityForResult(intent, FRAGMENT_REQUEST_CODE);
                        break;
                    case CRUISER:
                        intent = new Intent(activity, CruiserActivity.class);
                        intent.putExtra("shipDrawing", shipDrawing);
                        getParentFragment().startActivityForResult(intent, FRAGMENT_REQUEST_CODE);
                        break;
                    case DREADNOUGHT:
                        intent = new Intent(activity, DreadnoughtActivity.class);
                        intent.putExtra("shipDrawing", shipDrawing);
                        getParentFragment().startActivityForResult(intent, FRAGMENT_REQUEST_CODE);
                        break;
                    case STARBASE:
                        intent = new Intent(activity, StarbaseActivity.class);
                        intent.putExtra("shipDrawing", shipDrawing);
                        getParentFragment().startActivityForResult(intent, FRAGMENT_REQUEST_CODE);
                        break;
                }
            }
        });

        spinnerShipsAmount = (Spinner) rootView.findViewById(R.id.ship_amount);
        ArrayAdapter<?> dataAdapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, spinnerShipAmountArray);
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerShipsAmount.setAdapter(dataAdapter);
        return rootView;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null){
            fragmTag = savedInstanceState.getString("fragmTag");
            shipDrawing = (ShipDrawing)savedInstanceState.getSerializable("shipDrawing");
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra("shipDrawing", shipDrawing);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onStart");
        }
        if (shipDrawing != null) {
            if (Logs.showLogs) {
                Log.d(Tags.MY_TAG.getTag(), "shipDrawing not null");
            }
            LayoutInflater inflater = getActivity().getLayoutInflater();
            LinearLayout drawingContainer = (LinearLayout) rootView.findViewById(R.id.drawing_container);
            drawingContainer.removeAllViews();

            for (Drw armament : Drw.values()) {
                int amount = shipDrawing.getArmamentAmount(armament);
                if (amount > 0) {
                    View armamentView = inflater.inflate(R.layout.ships_drawing_part, null);
                    TextView armamentAmount = (TextView) armamentView.findViewById(R.id.armament_amount_text);
                    armamentAmount.setText("x" + amount);

                    ImageView armamentImg = (ImageView) armamentView.findViewById(R.id.armament_img);
                    armamentImg.setImageResource(armament.getIMG_SRC());
                    drawingContainer.addView(armamentView);
                }
            }
        }
    }

    @Override
    public void onResume() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onResume");
        }
        super.onResume();

    }

    @Override
    public void onResumeFragment() {

    }

    @Override
    public void onPauseFragment() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onPauseFragment");
        }
        String amount = spinnerShipAmountArray[spinnerShipsAmount.getSelectedItemPosition()];
        amount = amount.replace("x","");
        shipAmount = Integer.parseInt(amount);
    }

    @Override
    public void onPause() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onPause");
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onStop");
        }
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onSaveInstanceState");
        }
        super.onSaveInstanceState(outState);
        outState.putInt("FRAGMENT_REQUEST_CODE",FRAGMENT_REQUEST_CODE);
        outState.putString("fragmTag", fragmTag);
        outState.putSerializable("shipDrawing",shipDrawing);
    }

    @Override
    public void onDestroyView() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onDestroyView");
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass() + " onDestroy");
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), this.getClass() + " onActivityResult");
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FRAGMENT_REQUEST_CODE) {
                shipDrawing = (ShipDrawing) data.getSerializableExtra("shipDrawing");
                if (Logs.showLogs) {
                    Log.d(Tags.MY_TAG.getTag(), this.getClass() + " REQUEST_CODE AND RESULT_CODE OK ");
                }
            }
        }
    }

    public ShipFParams getShipType() {
        return shipType;
    }

    public ShipDrawing getShipDrawing() {
        return shipDrawing;
    }

    public int getShipAmount() {
        return shipAmount;
    }
}
