package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class InitiativeArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.INITIATIVE;


    public InitiativeArmament() {

        super(ARMAMENT_DRW);
    }
}
