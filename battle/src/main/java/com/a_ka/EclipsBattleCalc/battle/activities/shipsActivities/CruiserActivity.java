package com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities;


import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.DefaultDrawing;

public class CruiserActivity extends ShipActivity  {

    private final static int SHIP_HEADER_IMG = R.drawable.cruiser;
    private static DefaultDrawing defaultDrawing = DefaultDrawing.CRUISER;

    public CruiserActivity() {
        super(SHIP_HEADER_IMG, defaultDrawing);
    }
}
