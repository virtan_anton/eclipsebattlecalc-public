package com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities;


import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.DefaultDrawing;

public class StarbaseActivity extends ShipActivity  {

    private final static int SHIP_HEADER_IMG = R.drawable.starbase;
    private static DefaultDrawing defaultDrawing = DefaultDrawing.STARBASE;

    public StarbaseActivity() {
        super(SHIP_HEADER_IMG, defaultDrawing);
    }
}
