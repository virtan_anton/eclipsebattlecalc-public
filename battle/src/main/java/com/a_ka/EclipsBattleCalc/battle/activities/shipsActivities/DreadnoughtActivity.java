package com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities;


import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.DefaultDrawing;

public class DreadnoughtActivity extends ShipActivity  {

    private final static int SHIP_HEADER_IMG = R.drawable.dreadnought;
    private static DefaultDrawing defaultDrawing = DefaultDrawing.DREADNOUGHT;


    public DreadnoughtActivity() {
        super(SHIP_HEADER_IMG, defaultDrawing);
    }
}
