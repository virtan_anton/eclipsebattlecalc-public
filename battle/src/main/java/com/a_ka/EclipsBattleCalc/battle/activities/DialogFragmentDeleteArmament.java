package com.a_ka.EclipsBattleCalc.battle.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities.ShipActivity;


public class DialogFragmentDeleteArmament extends DialogFragment {
    private String fragmentTag;
    private ShipActivity activity;
    protected FragmentActivity mActivity;

    public DialogFragmentDeleteArmament(ShipActivity activity, String fragmentTag) {
        this.fragmentTag = fragmentTag;
        this.activity = activity;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(R.string.delete_armament_dialog_title)
                .setPositiveButton(R.string.delete_ship_dialog_positive_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.deleteArmamentFragment(fragmentTag);

                    }
                })
                .setNegativeButton(R.string.delete_ship_dialog_negative_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();
    }
}

