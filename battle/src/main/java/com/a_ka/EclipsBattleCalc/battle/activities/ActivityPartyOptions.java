package com.a_ka.EclipsBattleCalc.battle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.Tags;

public class ActivityPartyOptions extends ActionBarActivity {

    Toolbar toolbar;
    int partyOptionsFlag;
    LinearLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_options_layout);
        if (savedInstanceState != null){
            partyOptionsFlag = savedInstanceState.getInt("partyOptionsFlag");
        } else partyOptionsFlag = getIntent().getIntExtra("mPartyOptionsFlag", 0);


        Log.d(Tags.MY_TAG.getTag(), "ActivityPartyOptions. onCreate. partyOptionsFlag from Intent = " + partyOptionsFlag);
        container = (LinearLayout) findViewById(R.id.party_options_container);
        LayoutInflater inflater = getLayoutInflater();
        for (final PartyOptions options : PartyOptions.values()) {
            View v = inflater.inflate(R.layout.party_option_layout, null);

            ImageView optionImage = (ImageView) v.findViewById(R.id.option_img);
            optionImage.setImageResource(options.getOptionImgRes());
            TextView optionText = (TextView) v.findViewById(R.id.option_text);
            optionText.setText(options.getOptionStringRes());

            final CheckBox   chBox = (CheckBox) v.findViewById(R.id.option_ch_box);
            boolean hasOption = (partyOptionsFlag & options.getOptionFlag()) == options.getOptionFlag();
            if (hasOption) {
                chBox.setChecked(true);
            }
            chBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        partyOptionsFlag = partyOptionsFlag | options.getOptionFlag();
                    } else {
                        partyOptionsFlag = partyOptionsFlag ^ options.getOptionFlag();
                    }
                }
            });

            v.setClickable(true);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chBox.toggle();
                }
            });
            container.addView(v);
        }

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getIntExtra("titleResId", 0));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("partyOptionsFlag",partyOptionsFlag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ship_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ok:
                Intent intent = new Intent();
                intent.putExtra("mPartyOptionsFlag", partyOptionsFlag);
                setResult(RESULT_OK, intent);
                finish();
                break;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
