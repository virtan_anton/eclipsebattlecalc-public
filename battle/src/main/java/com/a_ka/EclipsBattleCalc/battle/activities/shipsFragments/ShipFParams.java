package com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments;


import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.DefaultDrawing;

public enum ShipFParams {

    INTERCEPTOR (R.drawable.interceptor, R.string.interceptor, new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8"},"INTERCEPTOR-11","INTERCEPTOR-21", DefaultDrawing.INTERCEPTOR, InterceptorFragment.class.getName() ),
    CRUISER(R.drawable.cruiser, R.string.cruiser, new String[]{"x1", "x2", "x3", "x4"},"CRUISER-12","CRUISER-22", DefaultDrawing.CRUISER, CruiserFragment.class.getName() ),
    DREADNOUGHT(R.drawable.dreadnought, R.string.dreadnought, new String[]{"x1", "x2"},"DREADNOUGHT-13","DREADNOUGHT-23", DefaultDrawing.DREADNOUGHT, DreadnoughtFragment.class.getName()),
    STARBASE(R.drawable.starbase, R.string.starbase, new String[]{"x1", "x2", "x3", "x4"}, "STARBASE-14", "STARBASE-24", DefaultDrawing.STARBASE, StarbaseFragment.class.getName());

    private final int IMG_SRC;
    private final int NAME_SRC;
    private final String[] SHIP_AMOUNT_LIST;
    private final String SHIP_TAG_1;
    private final String SHIP_TAG_2;
    private final DefaultDrawing defaultDrawing;
    private final String shipFname;



    ShipFParams(int IMG_SRC, int NAME_SRC, String[] SHIP_AMOUNT_LIST, String SHIP_TAG_1, String SHIP_TAG_2, DefaultDrawing defaultDrawing, String shipFname) {
        this.NAME_SRC = NAME_SRC;
        this.IMG_SRC = IMG_SRC;
        this.SHIP_AMOUNT_LIST = SHIP_AMOUNT_LIST;
        this.SHIP_TAG_1 = SHIP_TAG_1;
        this.SHIP_TAG_2 = SHIP_TAG_2;
        this.defaultDrawing = defaultDrawing;
        this.shipFname = shipFname;
    }

    public int getIMG_SRC() {
        return IMG_SRC;
    }

    public int getNAME_SRC() {
        return NAME_SRC;
    }

    public String[] getSHIP_AMOUNT_LIST() {
        return SHIP_AMOUNT_LIST;
    }

    public String getSHIP_TAG_1() {
        return SHIP_TAG_1;
    }

    public String getSHIP_TAG_2() {
        return SHIP_TAG_2;
    }

    public DefaultDrawing getDefaultShipDrawing() {
        return defaultDrawing;
    }

    public String getShipFname() {
        return shipFname;
    }
}
