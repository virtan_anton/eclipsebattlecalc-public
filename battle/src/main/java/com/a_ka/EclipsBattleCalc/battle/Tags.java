package com.a_ka.EclipsBattleCalc.battle;


public enum Tags {
    MY_TAG("my_tag"),
    CALC("calc"),
    SELECT_SHELLS("select_shells"),
    LIFE_CYCLE("life_cycle");

    private String tag;

    Tags(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}


