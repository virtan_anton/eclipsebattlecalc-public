package com.a_ka.EclipsBattleCalc.battle.pojo;


public class Shell implements Comparable<Shell> {

    private int aim = 0;
    private int damage;
    private int diveValue;
    private int computerValue;
    private boolean isIndicativeAim = false;
    private boolean hasPotentialAim = false;


    public Shell(int damage, int diveValue, int computerValue) {
        this.damage = damage;
        this.diveValue = diveValue;
        this.computerValue = computerValue;
    }


    public int getAim() {
        return aim;
    }

    public void setAim(int aim) {
        this.aim = aim;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getDiceValue() {
        return diveValue;
    }

    public void setDiveValue(int diveValue) {
        this.diveValue = diveValue;
    }

    public int getComputerValue() {
        return computerValue;
    }

    public void setComputerValue(int computerValue) {
        this.computerValue = computerValue;
    }

    public boolean isIndicativeAim() {
        return isIndicativeAim;
    }

    public void setIsIndicativeAim(boolean isIndicativeAim) {
        this.isIndicativeAim = isIndicativeAim;
    }

    public boolean isHasPotentialAim() {
        return hasPotentialAim;
    }

    public void setHasPotentialAim(boolean hasPotentialAim) {
        this.hasPotentialAim = hasPotentialAim;
    }

    @Override
    public String toString() {
        return " aim = " + aim + "; dmg = " + damage + "; diveVal = " + diveValue + "; compVal = " + computerValue;
    }

    @Override
    public int compareTo(Shell another) {
        return another.getDamage() - this.getDamage();
    }
}
