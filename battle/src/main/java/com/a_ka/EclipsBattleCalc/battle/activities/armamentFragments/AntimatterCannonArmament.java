package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class AntimatterCannonArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.ANTIMATTER_CANNON;

    public AntimatterCannonArmament() {
        super(ARMAMENT_DRW);
    }
}
