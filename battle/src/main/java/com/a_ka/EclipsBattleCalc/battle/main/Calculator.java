package com.a_ka.EclipsBattleCalc.battle.main;

import com.a_ka.EclipsBattleCalc.battle.activities.CalcRetainedFragment;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    private static int battleAmount = 1000;
    private Party[] parties = new Party[2];
    private Party[] startPartiesState = {null, null};
    List<Party> battleResults = new ArrayList<>();
    private float party1WinningPct = 0;
    private float party2WinningPct = 0;
    private float[] party1ExpectedLoss = new float[4];
    private float[] party2ExpectedLoss = new float[4];

    CalcRetainedFragment.AsyncCalc taskListener;


    public Calculator(CalcRetainedFragment.AsyncCalc task, Party party1, Party party2) {
        this.parties[0] = party1;
        this.parties[1] = party2;
        this.startPartiesState[0] = new Party(party1);
        this.startPartiesState[1] = new Party(party2);
        this.taskListener = task;
    }

    public boolean isCanceled() {
        return taskListener.isCancelled();
    }

    private int calcProgress(int battle, int battleAmount) {
        return (int) ((double) battle / (double) battleAmount * 100);
    }

    public void runBattles() {
        int battleN = 0;
        while (battleN <= battleAmount) {
            if (((startPartiesState[0].getFLAG() | startPartiesState[1].getFLAG()) & Party.FLAG_HAS_MISSILES) == Party.FLAG_HAS_MISSILES) {
                MissilePhase.runMissilePhase(parties);

                if (parties[0].getShipList().size() != 0 && parties[1].getShipList().size() != 0) {
                    EngagementRound.runEngagementRounds(parties);
                }
            } else {
                EngagementRound.runEngagementRounds(parties);
            }
            // write statistics to List
            if (parties[0].getShipList().size() != 0) {
                battleResults.add(parties[0]);
            } else {
                battleResults.add(parties[1]);
            }

            parties[0] = new Party(startPartiesState[0]);
            parties[1] = new Party(startPartiesState[1]);
            taskListener.doProgress(calcProgress(battleN, battleAmount));
            battleN++;
        }
//        battleN = 0;
    }

    public void calcPartiesWinningPct() {
        party1WinningPct = getPartyWinningPct(Party.PartyTag.PARTY1);
        party2WinningPct = getPartyWinningPct(Party.PartyTag.PARTY2);
    }

    public void calcPartiesExpectedLoss() {
        party1ExpectedLoss = getExpectedLoss(Party.PartyTag.PARTY1);
        party2ExpectedLoss = getExpectedLoss(Party.PartyTag.PARTY2);
    }

    private int getPartyWinningPct(Party.PartyTag partyTag) {
        float battleAmount = battleResults.size();
        float winAmount = 0;
        for (Party party : battleResults) {
            if (party != null) {
                if (party.getPartyTag() == partyTag) {
                    winAmount++;
                }
            }
        }
        return Math.round(winAmount / battleAmount * 100);
    }

    private float[] getExpectedLoss(Party.PartyTag partyTag) {
        int[] partyShipAmount;
        if (startPartiesState[0].getPartyTag() == partyTag) {
            partyShipAmount = getShipsAmountByType(startPartiesState[0]);
        } else {
            partyShipAmount = getShipsAmountByType(startPartiesState[1]);
        }
        float[] expectedLoss = new float[4];
        float[] expectedResidue = getExpectedResidue(partyTag);
        for (int i = 0; i < expectedLoss.length; i++) {
            expectedLoss[i] = partyShipAmount[i] - expectedResidue[i];
        }
        return expectedLoss;
    }

    private float[] getExpectedResidue(Party.PartyTag partyTag) {


        int[] residueInt = {0, 0, 0, 0};
        int amount = 0;
        for (Party resultParty : battleResults) {
            if (resultParty.getPartyTag() == partyTag) {
                residueInt = sumTwoArray(residueInt, getShipsAmountByType(resultParty));
                amount++;
            }
        }
        float[] residue = new float[4];
        for (int i = 0; i < residue.length; i++) {
            if (amount > 0) {
                residue[i] = (float) residueInt[i] / (float) amount;
            }
        }
        return residue;
    }

    public int[] sumTwoArray(int[] array1, int[] array2) {
        int[] shorterArray;
        int[] longerArray;
        int newArrayLength;
        if (array1.length >= array2.length) {
            newArrayLength = array1.length;
            longerArray = array1;
            shorterArray = array2;
        } else {
            newArrayLength = array2.length;
            longerArray = array2;
            shorterArray = array1;
        }
        int[] sumArray = new int[newArrayLength];
        for (int i = 0; i < newArrayLength; i++) {
            int i1;
            if (i < shorterArray.length) {
                i1 = shorterArray[i];
            } else i1 = 0;
            int i2 = longerArray[i];
            sumArray[i] = i1 + i2;

        }
        return sumArray;
    }


    public int[] getShipsAmountByType(Party party) {
        int[] shipsAmount = {0, 0, 0, 0};
        // shipsAmount[0] - interceptor
        // shipsAmount[1] - cruiser
        // shipsAmount[2] - dreadnought
        // shipsAmount[3] - starbase

        for (Ship ship : party.getShipList()) {
            switch (ship.getShipType()) {
                case INTERCEPTOR:
                    shipsAmount[0] += 1;
                    break;

                case CRUISER:
                    shipsAmount[1] += 1;
                    break;

                case DREADNOUGHT:
                    shipsAmount[2] += 1;
                    break;

                case STARBASE:
                    shipsAmount[3] += 1;
                    break;
            }
        }
        return shipsAmount;
    }

    public static int getBattleAmount() {
        return battleAmount;
    }

    public void setBattleAmount(int battleAmount) {
        this.battleAmount = battleAmount;
    }

    public float getParty1WinningPct() {
        return party1WinningPct;
    }

    public float getParty2WinningPct() {
        return party2WinningPct;
    }

    public float[] getParty1ExpectedLoss() {
        return party1ExpectedLoss;
    }

    public float[] getParty2ExpectedLoss() {
        return party2ExpectedLoss;
    }
}
