package com.a_ka.EclipsBattleCalc.battle.pojo.ships;


public class Dreadnought extends Ship {

    public Dreadnought() {
        super();
        this.setInitiative(1);
        this.setComputer(1);
        this.setIonCannon(2);
        this.setHull(2);
        this.setShipType(ShipType.DREADNOUGHT);
    }
}
