package com.a_ka.EclipsBattleCalc.battle.activities;


public interface FragmentLifecycle {
    void onPauseFragment();
    void onResumeFragment();
}
