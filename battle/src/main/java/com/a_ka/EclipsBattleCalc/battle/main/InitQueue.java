package com.a_ka.EclipsBattleCalc.battle.main;


import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;

import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class InitQueue {

    private Queue<QueueMember> queue = new PriorityQueue<>();


    public InitQueue(Party[] parties, ParseType mode) {
        if (mode == ParseType.FOR_ENGAGEMENT) {
            parsePartyForEngagement(parties[0]);
            parsePartyForEngagement(parties[1]);
        } else {
            parsePartyForMissiles(parties[0]);
            parsePartyForMissiles(parties[1]);
        }
    }

    private void parsePartyForEngagement(Party party) {
        for (Ship.ShipType type : Ship.ShipType.values()) {
            List<Ship> shipList = party.getShipsByType(type);
            if (shipList.size() > 0) {
                queue.add(new QueueMember(party, type, shipList.get(0).getInitiative()));
            }


        }
    }

    private void parsePartyForMissiles(Party party) {
        if ((party.getFLAG() & Party.FLAG_HAS_MISSILES) == Party.FLAG_HAS_MISSILES) {
            for (Ship.ShipType shipType : Ship.ShipType.values()) {
                List<Ship> shipList = party.getShipsByType(shipType);
                if (shipList.size() > 0 && ((shipList.get(0).getFluxMissiles() > 0) || (shipList.get(0).getPlasmaMissiles() > 0))) {
                    queue.add(new QueueMember(party, shipType, shipList.get(0).getInitiative()));
                }
            }
        }
    }

    public QueueMember getNextMember() {
        return queue.poll();
    }

    public enum ParseType {FOR_MISSILES, FOR_ENGAGEMENT}

    public class QueueMember implements Comparable<QueueMember> {
        private Party party;
        private Ship.ShipType shipType;
        private int shipTypeInitiative;

        public QueueMember(Party party, Ship.ShipType shipType, int shipTypeInitiative) {
            this.party = party;
            this.shipType = shipType;
            this.shipTypeInitiative = shipTypeInitiative;
        }

        public Party getParty() {
            return party;
        }


        public Ship.ShipType getShipType() {
            return shipType;
        }


        public int getShipTypeInitiative() {
            return shipTypeInitiative;
        }


        @Override
        public String toString() {
            return "QueueMember{" +
                    "partyTag=" + party +
                    ", shipType=" + shipType +
                    ", shipTypeInitiative=" + shipTypeInitiative +
                    '}';
        }

        @Override
        public int compareTo(QueueMember another) {
            int result = another.getShipTypeInitiative() - this.getShipTypeInitiative();
            if (result == 0) {
//                int thisInt = this.getParty().getFLAG() & 0b0001;
//                int anotherInt = another.getParty().getFLAG() & 0b0001;
//                Log.d(Tags.CALC.getTag(), "QueueMember, compareTo(): this=" + this+", another="+ another);
//                Log.d(Tags.CALC.getTag(), "QueueMember, compareTo(): thisInt=" +thisInt+ ", anotherInt="+anotherInt);
                return (another.getParty().getFLAG() & 0b0001) - (this.getParty().getFLAG() & 0b0001);
            } else return result;
        }
    }
}
