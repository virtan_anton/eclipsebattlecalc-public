package com.a_ka.EclipsBattleCalc.battle.main;


import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.Shell;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class BattlePhase {


    public static int getHighestInitiative(List<Ship> shipList) {
        int maxInitiative = 1;
        for (Ship ship : shipList) {
            if (ship.getInitiative() > maxInitiative) {
                maxInitiative = ship.getInitiative();
            }
        }
        return maxInitiative;
    }


    public static void sortPartiesByInit(Party[] partiesArray) {
        int p1Init = getHighestInitiative(partiesArray[0].getShipList());
        int p2Init = getHighestInitiative(partiesArray[1].getShipList());
        Party p1 = partiesArray[0];
        Party p2 = partiesArray[1];

        if (p1Init < p2Init) {
            partiesArray[0] = p2;
            partiesArray[1] = p1;
        } else {
            if (p1Init > p2Init) {
                partiesArray[0] = p1;
                partiesArray[1] = p2;
            } else {
                if ((p1.getFLAG() & Party.FLAG_DEFENDER) == Party.FLAG_DEFENDER) {
                    partiesArray[0] = p1;
                    partiesArray[1] = p2;
                } else {
                    partiesArray[0] = p2;
                    partiesArray[1] = p1;
                }
            }
        }
    }

}
