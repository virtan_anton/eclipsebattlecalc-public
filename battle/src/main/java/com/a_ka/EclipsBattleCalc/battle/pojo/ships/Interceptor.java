package com.a_ka.EclipsBattleCalc.battle.pojo.ships;


public class Interceptor extends Ship {



    public Interceptor() {
        super();
        this.setInitiative(3);
        this.setIonCannon(1);
        this.setShipType(ShipType.INTERCEPTOR);
    }


}
