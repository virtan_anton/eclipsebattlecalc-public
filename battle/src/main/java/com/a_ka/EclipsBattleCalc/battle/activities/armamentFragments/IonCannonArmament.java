package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class IonCannonArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.ION_CANNON;

    public IonCannonArmament() {
        super(ARMAMENT_DRW);
    }
}
