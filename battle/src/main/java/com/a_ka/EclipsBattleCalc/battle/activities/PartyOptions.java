package com.a_ka.EclipsBattleCalc.battle.activities;


import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;

public enum PartyOptions {
    DEFENDER(Party.FLAG_DEFENDER, R.drawable.defender, R.string.party_option_defender),
    POINT_DEFENSE(Party.FLAG_POINT_DEFENSE,R.drawable.point_defense, R.string.party_option_point_defense),
    DISTORTION_SHIELD(Party.FLAG_DISTORTION_SHIELD,R.drawable.distortion_shield, R.string.party_option_distortion_shield),
    ANTIMATTER_SPLITTER(Party.FLAG_ANTIMATTER_SPLITTER,R.drawable.antimatter_splitter, R.string.party_option_antimatter_splitter);

    private final int optionFlag;
    private  final int optionImgRes;
    private final int optionStringRes;

    PartyOptions(int optionFlag, int optionImgRes, int optionStringRes) {
        this.optionFlag = optionFlag;
        this.optionImgRes = optionImgRes;
        this.optionStringRes = optionStringRes;
    }

    public int getOptionFlag() {
        return optionFlag;
    }

    public int getOptionImgRes() {
        return optionImgRes;
    }

    public int getOptionStringRes() {
        return optionStringRes;
    }
}
