package com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments;

public class CruiserFragment extends ShipFragment{

    private final static ShipFParams SHIP_TYPE = ShipFParams.CRUISER;


    public CruiserFragment() {
        super(SHIP_TYPE);
    }
}
