package com.a_ka.EclipsBattleCalc.battle.activities;

import com.a_ka.EclipsBattleCalc.battle.R;


public class Tab1 extends TabParty {

    public Tab1() {
        super.addPartyOption(PartyOptions.DEFENDER.getOptionFlag());
        super.setPartyStringResId(R.string.party1_title);
        super.setPositionInPager(0);
    }


}
