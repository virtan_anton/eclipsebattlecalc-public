package com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments;


public class DreadnoughtFragment extends ShipFragment {

    private final static ShipFParams SHIP_TYPE = ShipFParams.DREADNOUGHT;


    public DreadnoughtFragment() {
        super(SHIP_TYPE);
    }
}
