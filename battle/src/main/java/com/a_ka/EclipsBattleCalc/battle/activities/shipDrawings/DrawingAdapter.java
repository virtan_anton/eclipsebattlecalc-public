package com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.Armament;

import java.util.ArrayList;
import java.util.List;

public class DrawingAdapter {


    public static ShipDrawing armamentsToDrawing(List<Fragment> armamentList) {
        ShipDrawing drawing = new ShipDrawing();
        for (Fragment armamentFragment : armamentList) {
            if (armamentFragment != null && armamentFragment.isVisible()) {
                Armament armament = (Armament) armamentFragment;
                drawing.addDrawing(armament.getArmamentDrw(), armament.getArmamentAmount());
                if (Logs.showLogs) {
                    Log.d(Tags.MY_TAG.getTag(), "Added armament: "+ armament.getArmamentDrw()+". Armament amount: "+armament.getArmamentAmount() );
                }
            }
        }
        return drawing;
    }

    public static List<Armament> drawingToArmament(Context context, ShipDrawing shipDrawing){
        List<Armament> fragmentList = new ArrayList<>();
        for (Drw armament : Drw.values()){
            int armamentAmount = shipDrawing.getArmamentAmount(armament);
            if (armamentAmount > 0){
                Armament armamentFragment = (Armament) Armament.instantiate(context, armament.getDrwFName());
                armamentFragment.setAmount(armamentAmount);
                fragmentList.add(armamentFragment);
            }
        }
        return fragmentList;
    }

//        public List<Armament> shipDrawingToArmament (ShipDrawing drawing){
//            List armamentList = new ArrayList();
//            if (drawing.isArmament(ShipDrawing.Drw.INITIATIVE)){
//
//            }
//        }


}
