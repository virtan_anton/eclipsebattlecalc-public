package com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities;


import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.DefaultDrawing;

public class InterceptorActivity extends ShipActivity  {

    private final static int SHIP_HEADER_IMG = R.drawable.interceptor;
    private static DefaultDrawing defaultDrawing = DefaultDrawing.INTERCEPTOR;

    public InterceptorActivity() {
        super(SHIP_HEADER_IMG, defaultDrawing);
    }
}
