package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class PlasmaCannonArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.PLASMA_CANNON;

    public PlasmaCannonArmament() {
        super(ARMAMENT_DRW);
    }
}
