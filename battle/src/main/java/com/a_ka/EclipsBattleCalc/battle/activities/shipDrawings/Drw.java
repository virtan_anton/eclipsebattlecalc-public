package com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings;


import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.AntimatterCannonArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.ComputerArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.FluxMissilesArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.HullArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.InitiativeArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.IonCannonArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.PlasmaCannonArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.PlasmaMissilesArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.RegenerationArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.ShieldArmament;

public enum Drw {
    INITIATIVE(R.drawable.initiative, R.string.initiative, new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9"}, InitiativeArmament.class.getName()),
    HULL(R.drawable.hull, R.string.hull, new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9"}, HullArmament.class.getName()),
    FLUX_MISSILES(R.drawable.flux_missiles, R.string.flux_missiles, new String[]{"x2", "x4", "x6", "x8"}, FluxMissilesArmament.class.getName()),
    PLASMA_MISSILES(R.drawable.plasma_missile, R.string.plasma_missile, new String[]{"x2", "x4", "x6", "x8"}, PlasmaMissilesArmament.class.getName()),
    COMPUTER(R.drawable.computer, R.string.computer, new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7"}, ComputerArmament.class.getName()),
    SHIELD(R.drawable.shield, R.string.shield, new String[]{"x1", "x2", "x3", "x4", "x5", "x6", "x7"}, ShieldArmament.class.getName()),
    ION_CANNON(R.drawable.ion_cannon, R.string.ion_cannon, new String[]{"x1", "x2", "x3", "x4", "x5"}, IonCannonArmament.class.getName()),
    PLASMA_CANNON(R.drawable.plasma_cannon, R.string.plasma_cannon, new String[]{"x1", "x2", "x3", "x4", "x5"}, PlasmaCannonArmament.class.getName()),
    ANTIMATTER_CANNON(R.drawable.antimatter_cannon, R.string.antimatter_cannon, new String[]{"x1", "x2", "x3", "x4", "x5"}, AntimatterCannonArmament.class.getName()),
    REGENERATION(R.drawable.regeneration, R.string.regeneration, new String[]{"x1", "x2", "x3"}, RegenerationArmament.class.getName());

    private final int IMG_SRC;
    private final int NAME_SRC;
    private final String[] ARMAMENT_AMOUNT_LIST;
    private final String drwFName;


    Drw(int IMG_SRC, int NAME_SRC, String[] ARMAMENT_AMOUNT_LIST, String drwFName) {
        this.IMG_SRC = IMG_SRC;
        this.NAME_SRC = NAME_SRC;
        this.ARMAMENT_AMOUNT_LIST = ARMAMENT_AMOUNT_LIST;
        this.drwFName = drwFName;

    }

    public int getIMG_SRC() {
        return IMG_SRC;
    }

    public int getNAME_SRC() {
        return NAME_SRC;
    }

    public String[] getARMAMENT_AMOUNT_LIST() {
        return ARMAMENT_AMOUNT_LIST;
    }

    public String getDrwFName() {
        return drwFName;
    }
}
