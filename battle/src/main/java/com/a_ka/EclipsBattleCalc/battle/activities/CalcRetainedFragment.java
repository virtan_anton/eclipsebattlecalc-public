package com.a_ka.EclipsBattleCalc.battle.activities;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.main.Calculator;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;

import java.util.ArrayList;
import java.util.List;

public class CalcRetainedFragment extends Fragment {

    private AsyncCalc asyncTask;

    private boolean isCalculatedResult = false;
    private Party calculatedParty1;
    private Party calculatedParty2;
    private int[] calculatedPartiesWinPrc;
    private float[] calculatedParty1ExpectedLoss;
    private float[] calculatedParty2ExpectedLoss;

    List<Fragment> party1NestedFragmentList = new ArrayList<>();
    List<Fragment> party2NestedFragmentList = new ArrayList<>();


    public interface TaskCallbacksListener {
        void onPreExecute();
        void onProgressUpdate(int percent);
        void onPostExecute();
    }

    TaskCallbacksListener taskCallbacksListener;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            taskCallbacksListener = (TaskCallbacksListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement TaskCallbacksListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }



    public void startAsyncCalc(Party party1, Party party2) {
        this.asyncTask = new AsyncCalc();
        asyncTask.execute(party1, party2);

    }

    public class AsyncCalc extends AsyncTask<Party, Integer, Void> {

        private Calculator calc;
        private Party calcP1;
        private Party calcP2;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (Logs.showLogs) {
                Log.d(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onPreExecute");
            }
            taskCallbacksListener.onPreExecute();
        }

        @Override
        protected Void doInBackground(Party... params) {
            calcP1 = new Party(params[0]);
            calcP2 = new Party(params[1]);
            calc = new Calculator(this, params[0], params[1]);
            if (Logs.showLogs) {
                Log.d(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " doInBackground");
                Log.d(Tags.CALC.getTag(), "doInBackground(), calcP1: " + calcP1);
                Log.d(Tags.CALC.getTag(), "doInBackground(), calcP2: " + calcP2);
            }
            calc.runBattles();
            calc.calcPartiesExpectedLoss();
            calc.calcPartiesWinningPct();
            return null;
        }


        @Override
        protected void onProgressUpdate(final Integer... values) {
            super.onProgressUpdate(values);
            if (Logs.showLogs) {
                Log.d(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onProgressUpdate");
                Log.d(Tags.CALC.getTag(), "onProgressUpdate: values[0] = " + values[0]);
            }
            taskCallbacksListener.onProgressUpdate(values[0]);

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            int[] partiesWinPrc = {(int) calc.getParty1WinningPct(), (int) calc.getParty2WinningPct()};
            if (Logs.showLogs) {
                Log.d(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onPostExecute");
                Log.d(Tags.CALC.getTag(), "onPostExecute(): Party1WinningPct = " + (int) calc.getParty1WinningPct() + ". Party2WinningPct = " + (int) calc.getParty2WinningPct());
                Log.d(Tags.CALC.getTag(), "onPostExecute(): Party1ExpectedLoss = [" + calc.getParty1ExpectedLoss()[0] + "][" + calc.getParty1ExpectedLoss()[1] + "][" + calc.getParty1ExpectedLoss()[2] + "][" + calc.getParty1ExpectedLoss()[3] + "]");
                Log.d(Tags.CALC.getTag(), "onPostExecute(): Party1ExpectedLoss = [" + calc.getParty2ExpectedLoss()[0] + "][" + calc.getParty2ExpectedLoss()[1] + "][" + calc.getParty2ExpectedLoss()[2] + "][" + calc.getParty2ExpectedLoss()[3] + "]");
            }
            isCalculatedResult = true;
            calculatedParty1 = calcP1;
            calculatedParty2 = calcP2;
            calculatedPartiesWinPrc = partiesWinPrc;
            calculatedParty1ExpectedLoss = calc.getParty1ExpectedLoss();
            calculatedParty2ExpectedLoss = calc.getParty2ExpectedLoss();
            taskCallbacksListener.onPostExecute();

        }

        public void doProgress(int currentProgress) {
            publishProgress(currentProgress);
        }


    }

    public AsyncCalc getAsyncTask() {
        return asyncTask;
    }

    public Party getCalculatedParty1() {
        return calculatedParty1;
    }

    public Party getCalculatedParty2() {
        return calculatedParty2;
    }

    public boolean isCalculatedResult() {
        return isCalculatedResult;
    }

    public void putParty1NestedFragmentList(List<Fragment> party1NestedFragmentList){
        this.party1NestedFragmentList = party1NestedFragmentList;
    }

    public void putParty2NestedFragmentList(List<Fragment> party2NestedFragmentList){
        this.party2NestedFragmentList = party2NestedFragmentList;
    }

    public List<Fragment> getParty1NestedFragmentList() {
        return party1NestedFragmentList;
    }

    public List<Fragment> getParty2NestedFragmentList() {
        return party2NestedFragmentList;
    }

    public int[] getCalculatedPartiesWinPrc() {
        return calculatedPartiesWinPrc;
    }

    public float[] getCalculatedParty1ExpectedLoss() {
        return calculatedParty1ExpectedLoss;
    }

    public float[] getCalculatedParty2ExpectedLoss() {
        return calculatedParty2ExpectedLoss;
    }
}
