package com.a_ka.EclipsBattleCalc.battle.pojo.ships;


public class Starbase extends Ship {

    public Starbase() {
        super();
        this.setInitiative(4);
        this.setIonCannon(1);
        this.setHull(2);
        this.setComputer(1);
        this.setShipType(ShipType.STARBASE);
    }
}
