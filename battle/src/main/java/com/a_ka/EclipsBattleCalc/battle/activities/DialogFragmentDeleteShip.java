package com.a_ka.EclipsBattleCalc.battle.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.a_ka.EclipsBattleCalc.battle.R;


public class DialogFragmentDeleteShip extends DialogFragment {
    private String fragmentTag;
    private ActivityMain activity;
    private int pagerPosition;

    public DialogFragmentDeleteShip(ActivityMain activity, int pagerPosition, String fragmentTag) {
        this.pagerPosition = pagerPosition;
        this.fragmentTag = fragmentTag;
        this.activity = activity;
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_ship_dialog_title)
                .setPositiveButton(R.string.delete_ship_dialog_positive_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.deleteFragment(fragmentTag, pagerPosition);

                    }
                })
                .setNegativeButton(R.string.delete_ship_dialog_negative_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();
    }
}

