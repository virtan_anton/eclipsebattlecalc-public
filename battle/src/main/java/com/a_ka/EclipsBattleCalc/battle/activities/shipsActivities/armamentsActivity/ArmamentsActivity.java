package com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities.armamentsActivity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.AntimatterCannonArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.Armament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.ComputerArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.FluxMissilesArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.HullArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.InitiativeArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.IonCannonArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.PlasmaCannonArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.PlasmaMissilesArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.RegenerationArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.ShieldArmament;

public class ArmamentsActivity extends Activity  implements View.OnClickListener {

        private LinearLayout initiative, hull, fluxMissiles, plasmaMissiles, computer, shield, ionCannon, plasmaCannon, antimatterCannon, regeneration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_armaments);

        initiative = (LinearLayout) findViewById(R.id.initiative);
        initiative.setOnClickListener(this);

        hull = (LinearLayout) findViewById(R.id.hull);
        hull.setOnClickListener(this);

        fluxMissiles = (LinearLayout) findViewById(R.id.fluxMissiles);
        fluxMissiles.setOnClickListener(this);

        plasmaMissiles = (LinearLayout) findViewById(R.id.plasmaMissiles);
        plasmaMissiles.setOnClickListener(this);

        computer= (LinearLayout) findViewById(R.id.computer);
        computer.setOnClickListener(this);

        shield= (LinearLayout) findViewById(R.id.shield);
        shield.setOnClickListener(this);

        ionCannon = (LinearLayout) findViewById(R.id.ionCannon);
        ionCannon.setOnClickListener(this);

        plasmaCannon= (LinearLayout) findViewById(R.id.plasmaCannon);
        plasmaCannon.setOnClickListener(this);

        antimatterCannon= (LinearLayout) findViewById(R.id.antimatterCannon);
        antimatterCannon.setOnClickListener(this);

        regeneration= (LinearLayout) findViewById(R.id.regeneration);
        regeneration.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        Armament armament;

        switch (v.getId()){
            case R.id.initiative:
                armament = new InitiativeArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.hull:
                armament = new HullArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.fluxMissiles:
                armament = new FluxMissilesArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.plasmaMissiles:
                armament = new PlasmaMissilesArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.computer:
                armament = new ComputerArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.shield:
                armament = new ShieldArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.ionCannon:
                armament = new IonCannonArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.plasmaCannon:
                armament = new PlasmaCannonArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.antimatterCannon:
                armament = new AntimatterCannonArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;

            case R.id.regeneration:
                armament = new RegenerationArmament();
                intent.putExtra("armament", armament);
                intent.putExtra("fragmentTag", armament.getFragmentTag());
                setResult(RESULT_OK, intent);
                finish();
                break;
            default:


        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }
}
