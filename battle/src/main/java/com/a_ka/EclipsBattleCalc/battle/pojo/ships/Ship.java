package com.a_ka.EclipsBattleCalc.battle.pojo.ships;


import android.os.Parcel;
import android.os.Parcelable;

import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.ShipDrawing;
import com.a_ka.EclipsBattleCalc.battle.pojo.D6Dice;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.Shell;

import java.util.ArrayList;
import java.util.List;

public class Ship implements Comparable, Parcelable {

    private int initiative;
    private int hull;
    private int shield;
    private int computer;
    private int regeneration;
    private int ionCannon;
    private int plasmaCannon;
    private int antimatterCannon;
    private int fluxMissiles;
    private int plasmaMissiles;
    private int ID;
    private static int LAST_ID = 0;
    private int FLAG;
    public static final int FLAG_DESTROYED = 0b01;
    public static final int FLAG_RETREATING = 0b10;
    private List<Shell> potentialShells = new ArrayList<>();
    private List<Shell> selectedShells = new ArrayList<>();
    private int obtainedDmg = 0;
    private ShipType shipType;


    public enum ShipType {
        INTERCEPTOR(1, "INTERCEPTOR"),
        CRUISER(3, "CRUISER"),
        DREADNOUGHT(4, "DREADNOUGHT"),
        STARBASE(2, "STARBASE");

        private int rating;
        private String shipName;

        ShipType(int rating, String shipName) {
            this.rating = rating;
            this.shipName = shipName;
        }


        public int getRating() {
            return rating;
        }

        public String getShipName() {
            return shipName;
        }

    }

    protected Ship() {
        this.setInitiative(0);
        this.setHull(0);
        this.setShield(0);
        this.setComputer(0);
        this.setRegeneration(0);
        this.setIonCannon(0);
        this.setPlasmaCannon(0);
        this.setAntimatterCannon(0);
        this.setFluxMissiles(0);
        this.setPlasmaMissiles(0);
        this.ID = ++LAST_ID;
    }

    public Ship(Ship another) {
        this.initiative = another.getInitiative();
        this.hull = another.getHull();
        this.shield = another.getShield();
        this.computer = another.getComputer();
        this.regeneration = another.getRegeneration();
        this.ionCannon = another.getIonCannon();
        this.plasmaCannon = another.getPlasmaCannon();
        this.antimatterCannon = another.getAntimatterCannon();
        this.fluxMissiles = another.getFluxMissiles();
        this.plasmaMissiles = another.getPlasmaMissiles();
        this.ID = another.getID();
        this.FLAG = another.getFLAG();
        this.potentialShells = new ArrayList<>();
        this.selectedShells = new ArrayList<>();
        this.obtainedDmg = another.getObtainedDmg();
        this.shipType = another.getShipType();
    }

    public void addPotentialShell(Shell shell) {
        potentialShells.add(shell);
    }

    public void addSelectedShell(Shell shell) {
        selectedShells.add(shell);
    }

    public void obtainDmg(int dmg) {
        obtainedDmg += dmg;
    }


    public List<Shell> missileShot() {
        List<Shell> list = new ArrayList<>();
        if (getPlasmaMissiles() != 0) {
            for (int f = 0; f < getPlasmaMissiles(); f++) {
                list.add(new Shell(2, D6Dice.roll(), getComputer()));
            }
        }
        if (getFluxMissiles() != 0) {
            for (int f = 0; f < getFluxMissiles(); f++) {
                list.add(new Shell(1, D6Dice.roll(), getComputer()));
            }
        }
        return list;
    }

    public List<Shell> makeCannonShotFrom(Party shootingParty) {
        List<Shell> list = new ArrayList<>();

        if (getIonCannon() != 0) {
            for (int f = 0; f < getIonCannon(); f++) {
                list.add(new Shell(1, D6Dice.roll(), getComputer()));
            }
        }

        if (getPlasmaCannon() != 0) {
            for (int f = 0; f < getPlasmaCannon(); f++) {
                list.add(new Shell(2, D6Dice.roll(), getComputer()));
            }
        }

        if (getAntimatterCannon() != 0) {
            if ((shootingParty.getFLAG() & Party.FLAG_ANTIMATTER_SPLITTER) == Party.FLAG_ANTIMATTER_SPLITTER) {
                for (int f = 0; f < getAntimatterCannon(); f++) {
                    int dice = D6Dice.roll();
                    list.add(new Shell(1, dice, getComputer()));
                    list.add(new Shell(1, dice, getComputer()));
                    list.add(new Shell(1, dice, getComputer()));
                    list.add(new Shell(1, dice, getComputer()));
                }
            } else {
                for (int f = 0; f < getAntimatterCannon(); f++) {
                    list.add(new Shell(4, D6Dice.roll(), getComputer()));
                }
            }
        }
        return list;
    }


    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Ship)) return false;
        Ship other = (Ship) o;
        return this.getShipType() == other.getShipType()
                && this.getInitiative() == other.getInitiative()
                && this.getHull() == other.getHull()
                && this.getFluxMissiles() == other.getFluxMissiles()
                && this.getPlasmaMissiles() == other.getPlasmaMissiles()
                && this.getComputer() == other.getComputer()
                && this.getShield() == other.getShield()
                && this.getIonCannon() == other.getIonCannon()
                && this.getPlasmaCannon() == other.getPlasmaCannon()
                && this.getAntimatterCannon() == other.getAntimatterCannon()
                && this.getRegeneration() == other.getRegeneration();
    }

    public void installDrawing(ShipDrawing drawing) {
        this.initiative = drawing.getArmamentAmount(Drw.INITIATIVE);
        this.hull = drawing.getArmamentAmount(Drw.HULL);
        this.fluxMissiles = drawing.getArmamentAmount(Drw.FLUX_MISSILES);
        this.plasmaMissiles = drawing.getArmamentAmount(Drw.PLASMA_MISSILES);
        this.computer = drawing.getArmamentAmount(Drw.COMPUTER);
        this.shield = drawing.getArmamentAmount(Drw.SHIELD);
        this.ionCannon = drawing.getArmamentAmount(Drw.ION_CANNON);
        this.plasmaCannon = drawing.getArmamentAmount(Drw.PLASMA_CANNON);
        this.antimatterCannon = drawing.getArmamentAmount(Drw.ANTIMATTER_CANNON);
        this.regeneration = drawing.getArmamentAmount(Drw.REGENERATION);

    }

    @Override
    public int compareTo(Object another) {
        Ship other = (Ship) another;
        return other.getShipType().getRating() - this.getShipType().getRating();
    }

    public int getInitiative() {
        return initiative;
    }

    public void setInitiative(int initiative) {
        this.initiative = initiative;
    }

    public int getHull() {
        return hull;
    }

    public void setHull(int hull) {
        this.hull = hull;
    }

    public int getShield() {
        return shield;
    }

    public void setShield(int shield) {
        this.shield = shield;
    }

    public int getComputer() {
        return computer;
    }

    public void setComputer(int computer) {
        this.computer = computer;
    }

    public int getRegeneration() {
        return regeneration;
    }

    public void setRegeneration(int regeneration) {
        this.regeneration = regeneration;
    }

    public int getIonCannon() {
        return ionCannon;
    }

    public void setIonCannon(int ionCannon) {
        this.ionCannon = ionCannon;
    }

    public int getPlasmaCannon() {
        return plasmaCannon;
    }

    public void setPlasmaCannon(int plasmaCannon) {
        this.plasmaCannon = plasmaCannon;
    }

    public int getAntimatterCannon() {
        return antimatterCannon;
    }

    public void setAntimatterCannon(int antimatterCannon) {
        this.antimatterCannon = antimatterCannon;
    }

    public int getFluxMissiles() {
        return fluxMissiles;
    }

    public void setFluxMissiles(int fluxMissiles) {
        this.fluxMissiles = fluxMissiles;
    }

    public int getPlasmaMissiles() {
        return plasmaMissiles;
    }

    public void setPlasmaMissiles(int plasmaMissiles) {
        this.plasmaMissiles = plasmaMissiles;
    }

    public int getID() {
        return ID;
    }

    public int getFLAG() {
        return FLAG;
    }

    public void setFLAG(int FLAG) {
        this.FLAG = FLAG;
    }

    public List<Shell> getPotentialShells() {
        return potentialShells;
    }

    public void setPotentialShells(List<Shell> potentialShells) {
        this.potentialShells = potentialShells;
    }

    public List<Shell> getSelectedShells() {
        return selectedShells;
    }

    public int getObtainedDmg() {
        return obtainedDmg;
    }

    public void setShipType(ShipType shipType) {
        this.shipType = shipType;
    }

    public ShipType getShipType() {
        return shipType;
    }

    public void setSelectedShells(List<Shell> selectedShells) {
        this.selectedShells = selectedShells;
    }

    @Override
    public String toString() {
        return " shipType: " + this.shipType.getShipName() + "; SHIP_ID = " + ID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}

