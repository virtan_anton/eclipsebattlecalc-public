package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;

import java.io.Serializable;

public abstract class Armament extends Fragment implements Serializable {


    public interface armamentFragmentListener {
        void onFragmentLongClickEvent(String fragmentTag);
    }

    armamentFragmentListener fragmentListener;

    private int armamentImg;
    private int armamentName;
    private String[] armamentAmountList;
    private Drw armamentDrw;
    private String fragmentTag;
    private int amount = 0;
    Spinner amountSpinner;

    public Armament(Drw drw) {
        this.armamentImg = drw.getIMG_SRC();
        this.armamentName = drw.getNAME_SRC();
        this.armamentAmountList = drw.getARMAMENT_AMOUNT_LIST();
        this.armamentDrw = drw;
        this.fragmentTag = drw.toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_armament, container, false);


        ImageView img = (ImageView) rootView.findViewById(R.id.armamentImg);
        img.setImageResource(armamentImg);

        TextView text = (TextView) rootView.findViewById(R.id.armamentString);
        text.setText(armamentName);

        amountSpinner = (Spinner) rootView.findViewById(R.id.armamentAmount);
        ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.simple_spinner_item, R.id.Text1, armamentAmountList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        amountSpinner.setAdapter(adapter);

        amountSpinner.setSelection(selectSpinnerPosition(amount, armamentAmountList));

        rootView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                fragmentListener.onFragmentLongClickEvent(fragmentTag);
                return true;
            }
        });

        return rootView;
    }

    private int selectSpinnerPosition(int armamentAmount, String[] armamentAmountList) {
        int position = 0;
        for (int i = 0; i < armamentAmountList.length; i++) {
            String listString = armamentAmountList[i];
            listString = listString.replace("x", "");
            int listInt = Integer.parseInt(listString);
            if (Logs.showLogs) {
                Log.d(Tags.MY_TAG.getTag(), "selectSpinnerPosition. armamentAmount = " + armamentAmount + ". listInt = " + listInt);
            }
            if (listInt == armamentAmount) {
                position = i;
            }
        }
        return position;
    }

    public int getArmamentAmount() {
        TextView text = (TextView) amountSpinner.getSelectedView().findViewById(R.id.Text1);
        String viewString = text.getText().toString();
        viewString = viewString.replace("x", "");
        return Integer.parseInt(viewString);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            fragmentListener = (armamentFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement armamentFragmentListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), this.getClass() + " onDestroy");
        }
    }

    public Drw getArmamentDrw() {
        return armamentDrw;
    }

    public String getFragmentTag() {
        return fragmentTag;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
