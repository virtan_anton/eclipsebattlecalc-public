package com.a_ka.EclipsBattleCalc.battle.main;

import android.util.Log;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.Shell;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EngagementRound extends BattlePhase {
    static int iLap = 1;
    private static InitQueue queue;
    private static Party[] mParties;
    private static Party attackingParty;
    private static Party defenderParty;
    private static List<Shell> cannonsDischarge = new ArrayList<>();


    public static void runEngagementRounds(Party[] parties) {
        mParties = parties;
        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), "         !!!RUN ENGAGEMENT ROUND. LAP: " + iLap + " !!!");
        }
        queue = new InitQueue(parties, InitQueue.ParseType.FOR_ENGAGEMENT);

        for (InitQueue.QueueMember member = queue.getNextMember(); member != null; ) {
            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), "                  !!! " + member.getParty().getPartyTag() + " ATTACK!!!");
            }
            queueMemberAttack(member);

            if (defenderParty.getShipList().size() == 0) {
                iLap = 1;
                return;
            }
            member = queue.getNextMember();
        }

        iLap++;
        runEngagementRounds(parties);

    }

    private static void queueMemberAttack(InitQueue.QueueMember queueMember) {

        Ship.ShipType attackingShipType = queueMember.getShipType();

        if (queueMember.getParty().getPartyTag() == Party.PartyTag.PARTY1) {
            attackingParty = mParties[0];
            defenderParty = mParties[1];
        } else {
            attackingParty = mParties[1];
            defenderParty = mParties[0];
        }
        List<Ship> attackingShips = attackingParty.getShipsByType(attackingShipType);
        if (attackingShips.isEmpty()) {
            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), queueMember + " HAS NO SHIPS!");
            }
            return;
        }
        if (hasShipCannon(attackingShips)) {
            String defPartyName = defenderParty.getPartyTag().toString();

            cannonsDischarge.clear();
            Collections.sort(defenderParty.getShipList());

            // get cannons discharge from attackingParty ships
            for (Ship ship : attackingParty.getShipsByType(attackingShipType)) {
                List<Shell> shellList = ship.makeCannonShotFrom(attackingParty);
                cannonsDischarge.addAll(shellList);

                if (Logs.showLogs) {
                    Log.d(Tags.CALC.getTag(), " cannons discharge from " + ship);
                    Log.d(Tags.CALC.getTag(), " shells: ");
                    for (Shell shell : shellList) {
                        Log.d(Tags.CALC.getTag(), shell.toString());
                    }
                }
            }
            Collections.sort(cannonsDischarge);

            // select potential shells for second party ships from cannon discharge
            AimSelection.setShipsPotentialShells(cannonsDischarge, defenderParty, EngagementRound.class);

            for (Ship ship : defenderParty.getShipList()) {
                AimSelection.selectShells(AimSelection.SelectionType.DESTROY, ship);
            }
            for (Ship ship : defenderParty.getShipList()) {
                AimSelection.selectShells(AimSelection.SelectionType.DAMAGE, ship);
            }

            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), defPartyName + " ships selected shells:");
                for (int iShip = 0; iShip < defenderParty.getShipList().size(); iShip++) {
                    Ship ship = defenderParty.getShipList().get(iShip);
                    Log.d(Tags.CALC.getTag(), defPartyName + " ship[" + iShip + "]: " + ship);
                    for (Shell shell : ship.getSelectedShells()) {
                        Log.d(Tags.CALC.getTag(), " Selected shell" + shell);
                    }
                }
            }
            // setting FLAG_DESTROYED, or obtain dmg to party ships
            defenderParty.setDmgOnShips();

            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), defPartyName + " obtained damage and destroyed ships:");
                for (int iShip = 0; iShip < defenderParty.getShipList().size(); iShip++) {
                    Ship ship = defenderParty.getShipList().get(iShip);
                    Log.d(Tags.CALC.getTag(), defPartyName + " ship[" + iShip + "]: " + ship);
                    Log.d(Tags.CALC.getTag(), " Destroyed: " + ((ship.getFLAG() & Ship.FLAG_DESTROYED) == Ship.FLAG_DESTROYED));
                    Log.d(Tags.CALC.getTag(), " Obtained damage: " + ship.getObtainedDmg());
                }
            }

            // remove defenderParty destroyed ships
            defenderParty.removeDestroyedShips();

            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), defPartyName + " remain ships:");
                for (int iShip = 0; iShip < defenderParty.getShipList().size(); iShip++) {
                    Ship ship = defenderParty.getShipList().get(iShip);
                    Log.d(Tags.CALC.getTag(), defPartyName + " ship[" + iShip + "]: " + ship);
                }
            }

        } else {
            if (isRetreat(attackingShips)) {
                destroyShips(attackingShips);
                attackingParty.removeDestroyedShips();
            } else retreatShips(attackingShips);
        }

    }

    private static boolean hasShipCannon(List<Ship> shipList) {
        Ship ship = shipList.get(0);
        boolean result = (ship.getIonCannon() > 0) || (ship.getPlasmaCannon() > 0) || (ship.getAntimatterCannon() > 0);
        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), "hasShipCannon(), return: " + result);
        }
        return result;
    }

    private static boolean isRetreat(List<Ship> shipList) {
        Ship ship = shipList.get(0);
        return (ship.getFLAG() & Ship.FLAG_RETREATING) == Ship.FLAG_RETREATING;
    }

    private static void destroyShips(List<Ship> shipList) {
        for (Ship ship : shipList) {
            ship.setFLAG(ship.getFLAG() | Ship.FLAG_DESTROYED);
        }
    }

    private static void retreatShips(List<Ship> shipList) {
        for (Ship ship : shipList) {
            ship.setFLAG(ship.getFLAG() | Ship.FLAG_RETREATING);
        }
    }

}

//
//List<Shell> cannonsDischarge = new ArrayList<>();
//    sortPartiesByInit(parties);
//Log.d(Tags.CALC.getTag(), " sortedParties[0]: " + parties[0].toString());
//        Log.d(Tags.CALC.getTag(), " sortedParties[1]: " + parties[1].toString());
//
//        //============================
//        // first party[0] action phase
//        //============================
//        Log.d(Tags.CALC.getTag(), "         !!!PARTY[0] ACTION PHASE!!!");
//
//        Collections.sort(parties[0].getShipList());
//        Collections.sort(parties[1].getShipList());
//
//        // get cannons discharge from party
//        Log.d(Tags.CALC.getTag(), " Get cannons discharge from party[0]");
//        for (Ship ship : parties[0].getShipList()) {
//        List<Shell> shellList = ship.makeCannonShotFrom(parties[0]);
//        cannonsDischarge.addAll(shellList);
//
//        Log.d(Tags.CALC.getTag(), " cannons discharge from " + ship);
//        Log.d(Tags.CALC.getTag(), " shells: ");
//        for (Shell shell : shellList) {
//        Log.d(Tags.CALC.getTag(), shell.toString());
//        }
//        }
//        Collections.sort(cannonsDischarge);
//
//        // select potential shells for second party ships from cannon discharge
//        AimSelection.setShipsPotentialShells(cannonsDischarge, parties[1], EngagementRound.class);
//
//        for (Ship ship : parties[1].getShipList()) {
//        AimSelection.selectShells(AimSelection.SelectionType.DESTROY, ship);
//        }
//        for (Ship ship : parties[1].getShipList()) {
//        AimSelection.selectShells(AimSelection.SelectionType.DAMAGE, ship);
//        }
//
//        Log.d(Tags.CALC.getTag(), " Party[1] ships selected shells:");
//        for (int iShip = 0; iShip < parties[1].getShipList().size(); iShip++) {
//        Ship ship = parties[1].getShipList().get(iShip);
//        Log.d(Tags.CALC.getTag(), " Party[1] ship[" + iShip + "]: " + ship);
//        for (Shell shell : ship.getSelectedShells()) {
//        Log.d(Tags.CALC.getTag(), " Selected shell" + shell);
//        }
//        }
//
//        // setting FLAG_DESTROYED, or obtain dmg to party ships
//        setDmgOnShips(parties[1]);
//
//        Log.d(Tags.CALC.getTag(), " Party[1] obtained damage and destroyed ships:");
//        for (int iShip = 0; iShip < parties[1].getShipList().size(); iShip++) {
//        Ship ship = parties[1].getShipList().get(iShip);
//        Log.d(Tags.CALC.getTag(), " Party[1] ship[" + iShip + "]: " + ship);
//        Log.d(Tags.CALC.getTag(), " Destroyed: " + ((ship.getFLAG() & Ship.FLAG_DESTROYED) == Ship.FLAG_DESTROYED));
//        Log.d(Tags.CALC.getTag(), " Obtained damage: " + ship.getObtainedDmg());
//        }
//
//        // remove party[1] destroyed ships
//        removeDestroyedShips(parties[1]);
//
//        Log.d(Tags.CALC.getTag(), " Party[1] remain ships:");
//        for (int iShip = 0; iShip < parties[1].getShipList().size(); iShip++) {
//        Ship ship = parties[1].getShipList().get(iShip);
//        Log.d(Tags.CALC.getTag(), " Party[1] ship[" + iShip + "]: " + ship);
//        }
//
//
//        if (parties[1].getShipList().size() == 0) {
//        iLap = 1;
//        return;
//        }
//
//        //=============================
//        // second party[1] action phase
//        //=============================
//        Log.d(Tags.CALC.getTag(), "         !!!PARTY[1] ACTION PHASE!!!");
//
//        Collections.sort(parties[0].getShipList());
//        Collections.sort(parties[1].getShipList());
//
//        cannonsDischarge.clear();
//
//        // get cannons discharge from party
//        Log.d(Tags.CALC.getTag(), " Get cannons discharge from party[1]");
//        for (Ship ship : parties[1].getShipList()) {
//        List<Shell> shellList = ship.makeCannonShotFrom(parties[1]);
//        cannonsDischarge.addAll(shellList);
//
//        Log.d(Tags.CALC.getTag(), " cannons discharge from " + ship);
//        Log.d(Tags.CALC.getTag(), " shells: ");
//        for (Shell shell : shellList) {
//        Log.d(Tags.CALC.getTag(), shell.toString());
//        }
//        }
//
//        // select potential shells for first party ships from cannon discharge
//        AimSelection.setShipsPotentialShells(cannonsDischarge, parties[0], EngagementRound.class);
//        for (Ship ship : parties[0].getShipList()) {
//        AimSelection.selectShells(AimSelection.SelectionType.DESTROY, ship);
//        }
//        for (Ship ship : parties[0].getShipList()) {
//        AimSelection.selectShells(AimSelection.SelectionType.DAMAGE, ship);
//        }
//
//        Log.d(Tags.CALC.getTag(), " Party[0] ships selected shells:");
//        for (int iShip = 0; iShip < parties[0].getShipList().size(); iShip++) {
//        Ship ship = parties[0].getShipList().get(iShip);
//        Log.d(Tags.CALC.getTag(), " Party[0] ship[" + iShip + "]: " + ship);
//        for (Shell shell : ship.getSelectedShells()) {
//        Log.d(Tags.CALC.getTag(), " Selected shell" + shell);
//        }
//        }
//
//        // setting FLAG_DESTROYED, or obtain dmg to party ships
//        setDmgOnShips(parties[0]);
//
//        Log.d(Tags.CALC.getTag(), " Party[0] obtained damage and destroyed ships:");
//        for (int iShip = 0; iShip < parties[0].getShipList().size(); iShip++) {
//        Ship ship = parties[0].getShipList().get(iShip);
//        Log.d(Tags.CALC.getTag(), " Party[0] ship[" + iShip + "]: " + ship);
//        Log.d(Tags.CALC.getTag(), " Destroyed: " + ((ship.getFLAG() & Ship.FLAG_DESTROYED) == Ship.FLAG_DESTROYED));
//        Log.d(Tags.CALC.getTag(), " Obtained damage: " + ship.getObtainedDmg());
//        }
//
//        // remove party[0] destroyed ships
//        removeDestroyedShips(parties[0]);
//
//        Log.d(Tags.CALC.getTag(), " Party[0] remain ships:");
//        for (int iShip = 0; iShip < parties[0].getShipList().size(); iShip++) {
//        Ship ship = parties[0].getShipList().get(iShip);
//        Log.d(Tags.CALC.getTag(), " Party[0] ship[" + iShip + "]: " + ship);
//        }
