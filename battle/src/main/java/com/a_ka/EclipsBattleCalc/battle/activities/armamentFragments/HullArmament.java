package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class HullArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.HULL;

    public HullArmament() {
        super(ARMAMENT_DRW);
    }
}
