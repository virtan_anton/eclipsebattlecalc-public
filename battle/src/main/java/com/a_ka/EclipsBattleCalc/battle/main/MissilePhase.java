package com.a_ka.EclipsBattleCalc.battle.main;


import android.util.Log;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.pojo.Party;
import com.a_ka.EclipsBattleCalc.battle.pojo.Shell;
import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MissilePhase extends BattlePhase {

    private static InitQueue queue;
    private static Party[] mParties;
    private static Party attackingParty;
    private static Party defenderParty;
    private static List<Shell> missilesDischarge = new ArrayList<>();


    public static void runMissilePhase(Party[] parties) {
        mParties = parties;
        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), "         !!!RUN MISSILES PHASE!!!");
        }

        queue = new InitQueue(parties, InitQueue.ParseType.FOR_MISSILES);

        for (InitQueue.QueueMember member = queue.getNextMember(); member != null; ) {
            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), "                  !!! " + member.getParty().getPartyTag() + " ATTACK!!!");
            }
            queueMemberAttack(member);
            if (defenderParty.getShipList().size() == 0) {
                return;
            }
            member = queue.getNextMember();
        }
    }

    private static void queueMemberAttack(InitQueue.QueueMember queueMember) {

        Ship.ShipType attackingShipType = queueMember.getShipType();

        if (queueMember.getParty().getPartyTag() == Party.PartyTag.PARTY1) {
            attackingParty = mParties[0];
            defenderParty = mParties[1];
        } else {
            attackingParty = mParties[1];
            defenderParty = mParties[0];
        }

        String defPartyName = defenderParty.getPartyTag().toString();

        missilesDischarge.clear();
        Collections.sort(defenderParty.getShipList());

        List<Ship> shootingShips = attackingParty.getShipsByType(attackingShipType);

        // get cannons discharge from attackingParty ships
        for (Ship ship : shootingShips) {
            List<Shell> shipDischarge = ship.missileShot();
            missilesDischarge.addAll(shipDischarge);

            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), " missiles discharge from " + ship);
                Log.d(Tags.CALC.getTag(), " missiles: ");
                for (Shell shell : shipDischarge) {
                    Log.d(Tags.CALC.getTag(), shell.toString());
                }
            }
        }

        Collections.sort(missilesDischarge);


        // select potential shells for second party ships from missile discharge
        AimSelection.setShipsPotentialShells(missilesDischarge, defenderParty, MissilePhase.class);

        if ((defenderParty.getFLAG() & Party.FLAG_POINT_DEFENSE) == Party.FLAG_POINT_DEFENSE) {
            removeMissilesWithoutPotentialAim(missilesDischarge);
            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), "Missiles with potential aim: " + missilesDischarge.size());
            }
            shootOnMissiles(defenderParty, missilesDischarge);
            AimSelection.setShipsPotentialShells(missilesDischarge, defenderParty, MissilePhase.class);
        }

        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), defPartyName + " ships potential shells:");
            for (int iShip = 0; iShip < defenderParty.getShipList().size(); iShip++) {
                Ship ship = defenderParty.getShipList().get(iShip);
                Log.d(Tags.CALC.getTag(), defPartyName + " ship[" + iShip + "]: " + ship);
                for (Shell shell : ship.getPotentialShells()) {
                    Log.d(Tags.CALC.getTag(), "Potential shell " + shell);
                }
            }
        }

        for (Ship ship : defenderParty.getShipList()) {
            AimSelection.selectShells(AimSelection.SelectionType.DESTROY, ship);
        }

        for (Ship ship : defenderParty.getShipList()) {
            AimSelection.selectShells(AimSelection.SelectionType.DAMAGE, ship);
        }

        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), defPartyName + " ships selected shells:");
            for (int iShip = 0; iShip < defenderParty.getShipList().size(); iShip++) {
                Ship ship = defenderParty.getShipList().get(iShip);
                Log.d(Tags.CALC.getTag(), defPartyName + " ship[" + iShip + "]: " + ship);
                for (Shell shell : ship.getSelectedShells()) {
                    Log.d(Tags.CALC.getTag(), "Selected shell" + shell);
                }
            }
        }


        // setting FLAG_DESTROYED, or obtain dmg to party ships
        defenderParty.setDmgOnShips();

        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), defPartyName + " obtained damage and destroyed ships:");
            for (int iShip = 0; iShip < defenderParty.getShipList().size(); iShip++) {
                Ship ship = defenderParty.getShipList().get(iShip);
                Log.d(Tags.CALC.getTag(), defPartyName + " ship[" + iShip + "]: " + ship);
                Log.d(Tags.CALC.getTag(), " Destroyed: " + ((ship.getFLAG() & Ship.FLAG_DESTROYED) == Ship.FLAG_DESTROYED));
                Log.d(Tags.CALC.getTag(), " Obtained damage: " + ship.getObtainedDmg());
            }
        }

        // update defenderParty after missile
        defenderParty.removeDestroyedShips();

        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), defPartyName + " remain ships:");
            for (int iShip = 0; iShip < defenderParty.getShipList().size(); iShip++) {
                Ship ship = defenderParty.getShipList().get(iShip);
                Log.d(Tags.CALC.getTag(), defPartyName + " ship[" + iShip + "]: " + ship);
            }
        }

    }


    private static void shootOnMissiles(Party shootingParty, List<Shell> missilesDischarge) {
        List<Shell> cannonsDischarge = new ArrayList<>();

        for (Ship ship : shootingParty.getShipList()) {
            List<Shell> shellList = ship.makeCannonShotFrom(shootingParty);
            cannonsDischarge.addAll(shellList);
        }
        Collections.sort(cannonsDischarge);

        int hitShellAmount = 0;
        for (Shell shell : cannonsDischarge) {
            if (isShellHitMissile(shell)) {
                hitShellAmount++;
            }
        }
        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), " shootOnMissiles(), hitShellAmount: " + hitShellAmount);
        }
        Collections.sort(missilesDischarge);
        if (hitShellAmount > missilesDischarge.size()) {
            hitShellAmount = missilesDischarge.size();
        }
        for (int iMissile = 0; iMissile < hitShellAmount; iMissile++) {
            missilesDischarge.remove(0);
        }
        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), " shootOnMissiles(), missiles lift : " + missilesDischarge.size());
        }
    }

    private static boolean isShellHitMissile(Shell shell) {
        return shell.getDiceValue() == 6 || shell.getDiceValue() != 1 && shell.getDiceValue() + shell.getComputerValue() >= 6;
    }

    private static void removeMissilesWithoutPotentialAim(List<Shell> missilesDischarge) {
        for (Iterator<Shell> iterator = missilesDischarge.iterator(); iterator.hasNext(); ) {
            if (!iterator.next().isHasPotentialAim()) iterator.remove();
        }
    }

}




