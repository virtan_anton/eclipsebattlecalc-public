package com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings;


import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;

public class ShipDrawing implements Serializable {


    private Map<Drw, Integer> drawingMap;


    public ShipDrawing() {
        drawingMap = new EnumMap<>(Drw.class);
    }

    public ShipDrawing(int initiativeAmount, int hullAmount, int fluxMissilesAmount, int plasmaMissilesAmount, int computerAmount, int shieldAmount, int ionCannonAmount, int plasmaCannonAmount, int antimatterCannonAmount, int regenerationAmount) {
        drawingMap = new EnumMap<>(Drw.class);
        drawingMap.put(Drw.INITIATIVE, initiativeAmount);
        drawingMap.put(Drw.HULL, hullAmount);
        drawingMap.put(Drw.FLUX_MISSILES, fluxMissilesAmount);
        drawingMap.put(Drw.PLASMA_MISSILES, plasmaMissilesAmount);
        drawingMap.put(Drw.COMPUTER, computerAmount);
        drawingMap.put(Drw.SHIELD, shieldAmount);
        drawingMap.put(Drw.ION_CANNON, ionCannonAmount);
        drawingMap.put(Drw.PLASMA_CANNON, plasmaCannonAmount);
        drawingMap.put(Drw.ANTIMATTER_CANNON, antimatterCannonAmount);
        drawingMap.put(Drw.REGENERATION, regenerationAmount);

    }

    public void addDrawing(Drw drawing, Integer amount) {
        drawingMap.put(drawing, amount);
    }

    public boolean isArmament(Drw armament) {
        return drawingMap.containsKey(armament);
    }

    public int getArmamentAmount(Drw armament) {
//        Log.d(Tags.MY_TAG.getTag(), armament + " amount: " + drawingMap.get(armament));
        if (drawingMap.get(armament) != null) {
            return drawingMap.get(armament);
        } else return 0;
    }

    public boolean hasMissiles(){
        int amount = getArmamentAmount(Drw.FLUX_MISSILES) + getArmamentAmount(Drw.PLASMA_MISSILES);
        return amount > 0;
    }

}
