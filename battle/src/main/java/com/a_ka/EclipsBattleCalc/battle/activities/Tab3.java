package com.a_ka.EclipsBattleCalc.battle.activities;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.R;
import com.github.clans.fab.FloatingActionButton;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Tab3 extends Fragment implements FragmentLifecycle {

    private int battleOldScrollY = 0;
    private FloatingActionButton fabCalc;

    private View v;
    LinearLayout linearLayoutContainer;
    LayoutInflater inflater;
    ViewGroup container;

    private int[] mPartiesWinPrc;
    private float[] mParty1ExpectedLoss;
    private float[] mParty2ExpectedLoss;

    public interface ResultUpdater {
        void updateResult();
    }

    ResultUpdater resultUpdater;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            resultUpdater = (ResultUpdater) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ResultUpdater");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onCreate");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onCreateView");
        }
        v = inflater.inflate(R.layout.tab_3, container, false);
        this.container = container;
        this.inflater = inflater;
        linearLayoutContainer = (LinearLayout) v.findViewById(R.id.battle_linearLayout_container);

        fabCalc = (FloatingActionButton) getActivity().findViewById(R.id.fab_battle);


        final ScrollView battleScrollView = (ScrollView) v.findViewById(R.id.battle_result_scroll_view);
        battleScrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        battleScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = battleScrollView.getScrollY();
                if (scrollY != battleOldScrollY) {
                    int scrollDY = battleOldScrollY - scrollY;
                    if (scrollDY < 0) {
                        fabCalc.setHideAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_out_bottom));
                        fabCalc.hide(true);
                        fabCalc.setHideAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_scale_down));
                    }
                    if (scrollDY > 0) {
                        fabCalc.setShowAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.abc_slide_in_bottom));
                        fabCalc.show(true);
                        fabCalc.setShowAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fab_scale_up));
                    }
                    battleOldScrollY = scrollY;
                }
            }
        });


        return v;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onViewStateRestored");
        }
        if (savedInstanceState != null) {
            if (savedInstanceState.getIntArray("partiesWinPrc") != null && savedInstanceState.getFloatArray("party1ExpectedLoss") != null && savedInstanceState.getFloatArray("party2ExpectedLoss") != null) {
                if (Logs.showLogs) {
                    Log.i(Tags.MY_TAG.getTag(), this.getClass().getName() + " publishBattleResult");
                }
                publishBattleResult(savedInstanceState.getIntArray("partiesWinPrc"), savedInstanceState.getFloatArray("party1ExpectedLoss"), savedInstanceState.getFloatArray("party2ExpectedLoss"));
            } else if (mPartiesWinPrc != null && mParty1ExpectedLoss != null && mParty2ExpectedLoss != null){
                if (Logs.showLogs) {
                    Log.i(Tags.MY_TAG.getTag(), this.getClass().getName() + " publishBattleResult from mVariables");
                }
                publishBattleResult(mPartiesWinPrc, mParty1ExpectedLoss, mParty2ExpectedLoss);
            }
        }
    }

    @Override
    public void onResumeFragment() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onResumeFragment");
        }
        resultUpdater.updateResult();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onSaveInstanceState");
        }
        super.onSaveInstanceState(outState);
        if (mPartiesWinPrc != null && mParty1ExpectedLoss != null && mParty2ExpectedLoss != null) {
            outState.putIntArray("partiesWinPrc", mPartiesWinPrc);
            outState.putFloatArray("party1ExpectedLoss", mParty1ExpectedLoss);
            outState.putFloatArray("party2ExpectedLoss", mParty2ExpectedLoss);
        }
    }

    @Override
    public void onPauseFragment() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onPauseFragment");
        }
    }

    @Override
    public void onPause() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onPause");
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onStop");
        }
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onDestroyView");
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onDestroy");
        }
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onDetach");
        }
        super.onDetach();
    }

    public void publishBattleResult(int[] partiesWinPrc, float[] party1ExpectedLoss, float[] party2ExpectedLoss) {
        if (Logs.showLogs) {
            Log.i(Tags.LIFE_CYCLE.getTag(), this.getClass().getName() + " onPublishBattleResult");
        }
        View nextLayout;
        ImageView imageView;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat round = new DecimalFormat("0.0", symbols);
        mPartiesWinPrc = partiesWinPrc;
        mParty1ExpectedLoss = party1ExpectedLoss;
        mParty2ExpectedLoss = party2ExpectedLoss;
        if (Logs.showLogs) {
            Log.d(Tags.CALC.getTag(), "publishBattleResult(): PARTY1. WinPrc: [" + partiesWinPrc[0] + "], ExpectedLoss: [" + party1ExpectedLoss[0] + "][" + party1ExpectedLoss[1] + "][" + party1ExpectedLoss[2] + "][" + party1ExpectedLoss[3] + "]");
            Log.d(Tags.CALC.getTag(), "publishBattleResult(): PARTY2. WinPrc: [" + partiesWinPrc[1] + "], ExpectedLoss: [" + party2ExpectedLoss[0] + "][" + party2ExpectedLoss[1] + "][" + party2ExpectedLoss[2] + "][" + party2ExpectedLoss[3] + "]");
        }

        if (linearLayoutContainer != null) {
            // remove run_battle_text
            linearLayoutContainer.removeAllViews();
            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), " publishBattleResult(). removeAllViews");
            }

            // winn pct title
            nextLayout = inflater.inflate(R.layout.battle_result_title, null);
            TextView textView = (TextView) nextLayout.findViewById(R.id.battle_result_title_text);
            textView.setText(R.string.result_winn_pct_title);
            linearLayoutContainer.addView(nextLayout);

            // winn pct by party
            nextLayout = inflater.inflate(R.layout.battle_result_two_column_text, null);
            textView = (TextView) nextLayout.findViewById(R.id.textParty1);
            textView.setText(partiesWinPrc[0] + "%");
            textView = (TextView) nextLayout.findViewById(R.id.textParty2);
            textView.setText(partiesWinPrc[1] + "%");
            linearLayoutContainer.addView(nextLayout);

            // expected losses title
            nextLayout = inflater.inflate(R.layout.battle_result_title, null);
            textView = (TextView) nextLayout.findViewById(R.id.battle_result_title_text);
            textView.setText(R.string.result_expected_losses);
            linearLayoutContainer.addView(nextLayout);

            // expected losses by party
            // INTERCEPTOR
            if ((party1ExpectedLoss[0] != 0) || (party2ExpectedLoss[0] != 0)) {
                nextLayout = inflater.inflate(R.layout.battle_result_two_column_text_img, null);
                textView = (TextView) nextLayout.findViewById(R.id.textParty1);
                if (party1ExpectedLoss[0] != 0)
                    textView.setText("x" + round.format(party1ExpectedLoss[0]));
                textView = (TextView) nextLayout.findViewById(R.id.textParty2);
                if (party2ExpectedLoss[0] != 0)
                    textView.setText("x" + round.format(party2ExpectedLoss[0]));
                imageView = (ImageView) nextLayout.findViewById(R.id.ship_image);
                imageView.setImageResource(R.drawable.interceptor);
                linearLayoutContainer.addView(nextLayout);
            }
            // CRUISER
            if ((party1ExpectedLoss[1] != 0) || (party2ExpectedLoss[1] != 0)) {
                nextLayout = inflater.inflate(R.layout.battle_result_two_column_text_img, null);
                textView = (TextView) nextLayout.findViewById(R.id.textParty1);
                if (party1ExpectedLoss[1] != 0)
                    textView.setText("x" + round.format(party1ExpectedLoss[1]));
                textView = (TextView) nextLayout.findViewById(R.id.textParty2);
                if (party2ExpectedLoss[1] != 0)
                    textView.setText("x" + round.format(party2ExpectedLoss[1]));
                imageView = (ImageView) nextLayout.findViewById(R.id.ship_image);
                imageView.setImageResource(R.drawable.cruiser);
                linearLayoutContainer.addView(nextLayout);
            }

            // DREADNOUGHT
            if ((party1ExpectedLoss[2] != 0) || (party2ExpectedLoss[2] != 0)) {
                nextLayout = inflater.inflate(R.layout.battle_result_two_column_text_img, null);
                textView = (TextView) nextLayout.findViewById(R.id.textParty1);
                if (party1ExpectedLoss[2] != 0)
                    textView.setText("x" + round.format(party1ExpectedLoss[2]));
                textView = (TextView) nextLayout.findViewById(R.id.textParty2);
                if (party2ExpectedLoss[2] != 0)
                    textView.setText("x" + round.format(party2ExpectedLoss[2]));
                imageView = (ImageView) nextLayout.findViewById(R.id.ship_image);
                imageView.setImageResource(R.drawable.dreadnought);
                linearLayoutContainer.addView(nextLayout);
            }

            // STARBASE
            if ((party1ExpectedLoss[3] != 0) || (party2ExpectedLoss[3] != 0)) {
                nextLayout = inflater.inflate(R.layout.battle_result_two_column_text_img, null);
                textView = (TextView) nextLayout.findViewById(R.id.textParty1);
                if (party1ExpectedLoss[3] != 0)
                    textView.setText("x" + round.format(party1ExpectedLoss[3]));
                textView = (TextView) nextLayout.findViewById(R.id.textParty2);
                if (party2ExpectedLoss[3] != 0)
                    textView.setText("x" + round.format(party2ExpectedLoss[3]));
                imageView = (ImageView) nextLayout.findViewById(R.id.ship_image);
                imageView.setImageResource(R.drawable.starbase);
                linearLayoutContainer.addView(nextLayout);
            }
            if (Logs.showLogs) {
                Log.d(Tags.CALC.getTag(), " publishBattleResult(). finish.");
            }
        }
    }

    public void clearResult() {
        mPartiesWinPrc = null;
        mParty1ExpectedLoss = null;
        mParty2ExpectedLoss = null;
        linearLayoutContainer.removeAllViews();
        inflater.inflate(R.layout.battle_run_battle_text, linearLayoutContainer);
    }

}

