package com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.a_ka.EclipsBattleCalc.battle.Logs;
import com.a_ka.EclipsBattleCalc.battle.Tags;
import com.a_ka.EclipsBattleCalc.battle.R;
import com.a_ka.EclipsBattleCalc.battle.activities.DialogFragmentDeleteArmament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.Armament;
import com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments.Armament.armamentFragmentListener;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.DefaultDrawing;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.DrawingAdapter;
import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.ShipDrawing;
import com.a_ka.EclipsBattleCalc.battle.activities.shipsActivities.armamentsActivity.ArmamentsActivity;
import com.github.clans.fab.FloatingActionButton;

import java.util.List;

public abstract class ShipActivity extends ActionBarActivity implements armamentFragmentListener {


    Toolbar toolbar;
    private int headerImgID;
    private DefaultDrawing defaultShipDrawing;
    private FloatingActionButton fab;
    private ImageView headerImage;
    private ImageView shipDrwSet;
    private LinearLayout armamentContainer;


    private FragmentManager fManager;
    private ShipDrawing shipDrawing;
    List<Fragment> armamentList;

    private int oldScrollY = 0;


    public ShipActivity(int headerImgID, DefaultDrawing defaultDrawing) {
        this.headerImgID = headerImgID;
        this.defaultShipDrawing = defaultDrawing;
    }

    public void deleteArmamentFragment(String fragmentTag) {
        fManager = getSupportFragmentManager();
        fManager.beginTransaction().detach(getSupportFragmentManager().findFragmentByTag(fragmentTag)).commit();
    }

    private void deleteAllArmamentFragments() {
        armamentList = fManager.getFragments();
        if (armamentList.size() > 0) {
            for (Fragment armamentFragment : armamentList) {
                fManager.beginTransaction().detach(armamentFragment).commit();
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship);

        shipDrawing = (ShipDrawing) getIntent().getSerializableExtra("shipDrawing");
        List<Armament> armamentList = DrawingAdapter.drawingToArmament(this, shipDrawing);

        fManager = getSupportFragmentManager();

        armamentContainer = (LinearLayout) findViewById(R.id.armament_container);
        placeArmamentFragments(armamentContainer, armamentList);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ArmamentsActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        headerImage = (ImageView) findViewById(R.id.headerImg);
        headerImage.setImageResource(headerImgID);

        final ScrollView armamentScrollView = (ScrollView) findViewById(R.id.armament_scroll_view);
        armamentScrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        armamentScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {

                int scrollY = armamentScrollView.getScrollY();
                if (scrollY != oldScrollY) {
                    int scrollDY = oldScrollY - scrollY;
                    if (scrollDY < 0) {
                        fab.hide(true);
                    }
                    if (scrollDY > 0) {
                        fab.show(true);
                    }
                    oldScrollY = scrollY;

                }
            }
        });

        shipDrwSet = (ImageView) findViewById(R.id.drw_set);
        shipDrwSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Armament> armamentList = DrawingAdapter.drawingToArmament(ShipActivity.this, defaultShipDrawing.getDrawing());
                placeArmamentFragments(armamentContainer, armamentList);
            }
        });


    }

    private void placeArmamentFragments(ViewGroup container, List<Armament> armamentList) {
        if (fManager.getFragments()!= null) {
            Log.d(Tags.MY_TAG.getTag(), "container.getChildCount() > 0");
            deleteAllArmamentFragments();
        }
        for (Armament fragment : armamentList) {
            if (Logs.showLogs) {
                Log.d(Tags.MY_TAG.getTag(), fragment.getClass() + " add");
            }
            fManager.beginTransaction().add(container.getId(), fragment, fragment.getFragmentTag()).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_ship_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ok:
                Intent intent = new Intent();
                armamentList = fManager.getFragments();
                shipDrawing = DrawingAdapter.armamentsToDrawing(armamentList);
                intent.putExtra("shipDrawing", shipDrawing);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            case android.R.id.home:
                if (Logs.showLogs) {
                    Log.d(Tags.MY_TAG.getTag(), this.getClass() + " onOptionsItemSelected: home");
                }
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Armament newArmament = (Armament) data.getSerializableExtra("armament");
            String fragmentTag = data.getStringExtra("fragmentTag");
            Fragment tempFragment = fManager.findFragmentByTag(fragmentTag);
            if (tempFragment == null) {
                fManager.beginTransaction().add(R.id.armament_container, newArmament, fragmentTag).addToBackStack(fragmentTag).commit();
            } else {
                if (tempFragment.isDetached()) {
                    fManager.beginTransaction().attach(tempFragment).addToBackStack(fragmentTag).commit();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onFragmentLongClickEvent(String fragmentTag) {
        deleteArmamentFragment(fragmentTag);
        Toast.makeText(this,R.string.delete_armament_toast,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Logs.showLogs) {
            Log.d(Tags.MY_TAG.getTag(), this.getClass() + " onDestroy");
        }
    }
}
