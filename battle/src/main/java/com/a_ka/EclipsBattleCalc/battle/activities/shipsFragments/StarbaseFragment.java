package com.a_ka.EclipsBattleCalc.battle.activities.shipsFragments;

public class StarbaseFragment extends ShipFragment{

    private final static ShipFParams SHIP_TYPE = ShipFParams.STARBASE;


    public StarbaseFragment() {
        super(SHIP_TYPE);
    }
}
