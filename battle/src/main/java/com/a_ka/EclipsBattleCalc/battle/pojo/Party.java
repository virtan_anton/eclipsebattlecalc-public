package com.a_ka.EclipsBattleCalc.battle.pojo;


import com.a_ka.EclipsBattleCalc.battle.pojo.ships.Ship;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Party implements Serializable {

    private int FLAG;

    public static final int FLAG_DEFENDER = 1; // 0001
    public static final int FLAG_POINT_DEFENSE = 2; // 0010
    public static final int FLAG_DISTORTION_SHIELD = 4; // 0100
    public static final int FLAG_ANTIMATTER_SPLITTER = 8; // 1000

    public static final int FLAG_HAS_MISSILES = 16; // 0001 0000

    private List<Ship> shipList;
    private  PartyTag partyTag;


    public Party(int FLAG, List<Ship> shipList, PartyTag partyTag) {
        this.FLAG = FLAG;
        this.shipList = shipList;
        this.partyTag = partyTag;
    }

    public Party (Party another){
        this.FLAG = another.getFLAG();
        this.partyTag = another.getPartyTag();
        List<Ship> shipList = new ArrayList<>(another.getShipList().size());
        for (Ship ship: another.getShipList()) shipList.add(new Ship(ship));
        this.shipList = shipList;
    }

    public int getFLAG() {
        return FLAG;
    }

    public int getFLAG_DISTORTION_SHIELD_BONUS() {
        if ((FLAG & Party.FLAG_DISTORTION_SHIELD) == FLAG_DISTORTION_SHIELD) {
            return -2;
        }
        return 0;
    }

    public List<Ship> getShipsByType(Ship.ShipType shipType) {
        List<Ship> shipList = new ArrayList<>();
        for (Ship ship : this.getShipList()) {
            if (ship.getShipType() == shipType) {
                shipList.add(ship);
            }
        }
        return shipList;
    }

    public boolean hasPartyShipType(Ship.ShipType shipType) {
        return getShipsByType(shipType).size() > 0;
    }

    public void removeDestroyedShips() {
        List<Ship> shipList = this.getShipList();
        for(Iterator<Ship> it = shipList.iterator(); it.hasNext();){
            if ((it.next().getFLAG() & Ship.FLAG_DESTROYED) == Ship.FLAG_DESTROYED) it.remove();
        }
    }

    public void setDmgOnShips() {
        for (Ship ship : this.getShipList()) {
            // get sum dmg of ships selected shells
            int sumDmg = 0;
            for (Shell shell : ship.getSelectedShells()) {
                sumDmg += shell.getDamage();
            }
            if (sumDmg >= ship.getHull() + 1 - ship.getObtainedDmg()) {
                ship.setFLAG(ship.getFLAG() | Ship.FLAG_DESTROYED);
            } else {
                ship.obtainDmg(sumDmg);
                ship.getPotentialShells().clear();
                ship.getSelectedShells().clear();
            }
        }
    }

    public enum PartyTag implements Cloneable {PARTY1, PARTY2}

    public PartyTag getPartyTag() {
        return partyTag;
    }

    public List<Ship> getShipList() {
        return shipList;
    }

    public void setShipList(List<Ship> shipList) {
        this.shipList = shipList;
    }

    public void addShip(Ship ship) {
        shipList.add(ship);
    }

    public void setFLAG(int FLAG) {
        this.FLAG = FLAG;
    }

    @Override
    public String toString() {
        return " PartyTag: " + partyTag + ". FLAG: " + Integer.toBinaryString(FLAG) + ". ships: " + shipList.size();
    }


}
