package com.a_ka.EclipsBattleCalc.battle.activities.armamentFragments;


import com.a_ka.EclipsBattleCalc.battle.activities.shipDrawings.Drw;


public class ShieldArmament extends Armament {

    private final static Drw ARMAMENT_DRW = Drw.SHIELD;

    public ShieldArmament() {
        super(ARMAMENT_DRW);
    }
}
